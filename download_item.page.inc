<?php

/**
 * @file
 * Contains download_item.page.inc.
 *
 * Page callback for Download item entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Download item templates.
 *
 * Default template: download_item.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_download_item(array &$variables) {
  // Fetch DownloadItem Entity Object.
  $download_item = $variables['elements']['#download_item'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
