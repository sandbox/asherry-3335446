<?php

/**
 * @file
 * Contains download_request.page.inc.
 *
 * Page callback for Download request entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Download request templates.
 *
 * Default template: download_request.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_download_request(array &$variables) {
  // Fetch DownloadRequest Entity Object.
  $download_request = $variables['elements']['#download_request'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
