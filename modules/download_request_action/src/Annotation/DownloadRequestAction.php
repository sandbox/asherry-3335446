<?php

namespace Drupal\download_request_action\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Download request action type item annotation object.
 *
 * @see \Drupal\download_request_action\DownloadRequestActionManager
 * @see plugin_api
 *
 * @Annotation
 */
class DownloadRequestAction extends Plugin {


  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

}
