<?php

namespace Drupal\download_request_action;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\entity\Routing\AdminHtmlRouteProvider;

/**
 * Provides routes for Download request email action entities.
 *
 * @see Drupal\Core\Entity\Routing\AdminHtmlRouteProvider
 * @see Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider
 */
class DownloadRequestActionHtmlRouteProvider extends AdminHtmlRouteProvider {

  /**
   * {@inheritdoc}
   */
  public function getRoutes(EntityTypeInterface $entity_type) {
    $collection = parent::getRoutes($entity_type);

    foreach ($collection as $route)  {
      $params = $route->getOption('parameters');
      $params['download_request_type'] = [
        'type' => 'entity:download_request_type'
      ];
      $route->setOption('parameters', $params);
    }

    // Provide your custom entity routes here.
    return $collection;
  }

}
