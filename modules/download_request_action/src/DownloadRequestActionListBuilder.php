<?php

namespace Drupal\download_request_action;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Config\Entity\DraggableListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\entity\BulkFormEntityListBuilder;

/**
 * Provides a listing of Download request action entities.
 */
class DownloadRequestActionListBuilder extends DraggableListBuilder {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'download_request_action_list_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Download request action');
    $header['status'] = $this->t('Status');
    $header['id'] = $this->t('Machine name');
    $header['type'] = $this->t('Type');
    $header['transition_id'] = $this->t('Transition ID');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['label'] = $entity->label();
    $row['status'] = [
      '#type' => 'checkbox',
      '#default_value' => $entity->status(),
    ];
    $row['id'] = ['#markup' => $entity->id()];
    $row['type'] = ['#markup' => $entity->getPlugin()->label()];
    $row['transition_id'] = ['#markup' => $entity->getTransitionId()];
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    foreach ($form_state->getValue($this->entitiesKey) as $id => $value) {
      if (isset($this->entities[$id]) && $this->entities[$id]->status() != $value['status']) {
        // Save entity only when its weight was changed.
        $this->entities[$id]->setStatus($value['status']);
        $this->entities[$id]->save();
      }
    }
    parent::submitForm($form, $form_state);
  }
}
