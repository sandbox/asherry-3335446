<?php

namespace Drupal\download_request_action;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\download_request_action\EventSubscriber\DownloadRequestActionEventSubscriber;

/**
 * Current functionality run directly in EventSubscriber.
 *
 * TODO:
 * - We may as well add a transition to a DownloadRequest entity
 * - Change the DownloadRequestActionForm to target either
 *   - download_request => Download Request
 *   - download_request_item => Download Request Item
 * - 'transition_entity_type'
 * - 'transition_field_name'
 * - Then pick a state field, (could hard code first but then get a dropdown)
 * - Then keep transition_id
 *
 * @see DownloadRequestActionEventSubscriber
 */
class DownloadRequestActionManager extends DefaultPluginManager {

  use DependencySerializationTrait;

  /**
   * @var EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $downloadRequestActionStorage;

  /**
   * Constructs a new DownloadRequestActionManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   * @param EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct('Plugin/DownloadRequest/Action', $namespaces, $module_handler, 'Drupal\download_request_action\Plugin\DownloadRequestActionInterface', 'Drupal\download_request_action\Annotation\DownloadRequestAction');

    $this->alterInfo('download_request_action_download_request_action_type_info');
    $this->setCacheBackend($cache_backend, 'download_request_action_download_request_action_type_plugins');
    $this->entityTypeManager = $entity_type_manager;
    $this->downloadRequestActionStorage = $this->entityTypeManager->getStorage('download_request_action');
  }

  /**
   * @param $type
   * @param $entity
   */
  public function trigger($type, EntityInterface $entity) {
    $this->doTriggerActions($type, $entity);
  }

  /**
   * Get an array of download request item entities keyed by action id.
   *
   * @param EntityInterface $entity
   *   The entity that has triggered the actions.
   * @return array
   *   Groups applicable download_request_item ids by action config entity id.
   */
  private function doGetActions($type, EntityInterface $entity) {
    $action_request_items = [];
    // TODO: Decide if we should search for download_request criteria, AND download_request_item
    // criteria. We might just decide to say item is fine? The system is currently only using
    // download_request_items;
    $entity_type_id = $entity->getEntityTypeId();
    // Extract all the download request items, or, if it's a
    // download_request_item that triggered this query in the first place then
    // just make an array of it.
    $request_items = $entity_type_id == 'download_request_item'
      ? [$entity->id() => $entity]
      : $entity->request_items->referencedEntities();

    foreach ($request_items as $download_request_item) {
      // Run a query, this can be customizable more once the properties of each
      // action is more customizable.
      $query = $this->downloadRequestActionStorage->getQuery()
        ->condition('transition_id', $type)
        ->condition('status', 1);

      // For now, the main  functionality lies in an alter, and a third party setting
      // to store what is queried.
      $this->moduleHandler->alter('download_request_action_event_query', $query, $download_request_item, $type);

      foreach ($query->execute() as $action_id) {
        if (!isset($action_request_items[$action_id])) {
          $action_request_items[$action_id] = [];
        }
        $action_request_items[$action_id][] = $download_request_item;
      }
    }
    return $action_request_items;
  }

  protected function doTriggerActions($type, EntityInterface $entity) {
    $entity_type_id = $entity->getEntityTypeId();
    $action_request_items = $this->doGetActions($type, $entity);

    $actions = [];
    if (!empty($action_request_items)) {
      $actions = $this->downloadRequestActionStorage->loadMultiple(array_keys($action_request_items));
    }

    foreach ($actions as $action) {
      $request_items = $action_request_items[$action->id()];
      $entity->limitItems = $request_item_ids = array_map(function($value) { return $value->id(); }, $request_items);
      $action->getPlugin()
        ->setContextValue($entity_type_id, $entity)
        ->execute();
    }
  }
}
