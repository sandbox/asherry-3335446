<?php

namespace Drupal\download_request_action\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Plugin\DefaultSingleLazyPluginCollection;

/**
 * Defines the Download request email action entity.
 *
 * @ConfigEntityType(
 *   id = "download_request_action",
 *   label = @Translation("Download request action"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\download_request_action\DownloadRequestActionListBuilder",
 *     "form" = {
 *       "add" = "Drupal\download_request_action\Form\DownloadRequestActionForm",
 *       "edit" = "Drupal\download_request_action\Form\DownloadRequestActionForm",
 *       "delete" = "Drupal\download_request_action\Form\DownloadRequestActionDeleteForm",
 *       "duplicate" = "\Drupal\download_request_action\Form\DownloadRequestActionForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\download_request_action\DownloadRequestActionHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "download_request_action",
 *   config_export = {
 *     "id",
 *     "label",
 *     "status",
 *     "weight",
 *     "dependencies",
 *     "third_party_settings",
 *     "plugin",
 *     "download_request_type",
 *     "transition_id",
 *     "settings"
 *   },
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "status" = "status",
 *     "weight" = "weight",
 *     "uuid" = "uuid",
 *     "download_request_type" = "download_request_type",
 *     "transition_id" = "transition_id"
 *   },
 *   links = {
 *     "canonical"      = "/admin/structure/download_request_type/{download_request_type}/actions/{download_request_action}",
 *     "add-form"       = "/admin/structure/download_request_type/{download_request_type}/actions/add",
 *     "edit-form"      = "/admin/structure/download_request_type/{download_request_type}/actions/{download_request_action}/edit",
 *     "delete-form"    = "/admin/structure/download_request_type/{download_request_type}/actions/{download_request_action}/delete",
 *     "duplicate-form" = "/admin/structure/download_request_type/{download_request_type}/actions/{download_request_action}/clone",
 *     "collection"     = "/admin/structure/download_request_type/{download_request_type}/actions"
 *   }
 * )
 */
class DownloadRequestAction extends ConfigEntityBase implements DownloadRequestActionInterface {

  /**
   * The Download request email action ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Download request email action label.
   *
   * @var string
   */
  protected $label;

  /**
   * The weight of the action when triggered.
   *
   * TODO: This needs to be added to the query.
   */
  protected $weight;

  /**
   * The plugin instance ID.
   *
   * @var string
   */
  protected $plugin;

  /**
   * @var DefaultSingleLazyPluginCollection
   */
  protected $pluginCollection;

  /**
   * Parent DownloadRequestType bundle.
   *
   * @var string
   */
  protected $download_request_type;

  /**
   * Workflow transition id
   *
   * @var string
   */
  protected $transition_id;

  /**
   * @var array
   */
  protected $settings = [];

  public function getWeight() {
    return $this->weight;
  }

  public function setWeight($weight) {
    $this->weight = $weight;
  }

  public function getSettings() {
    return $this->settings;
  }

  public function setSettings($settings) {
    $this->settings = $settings;
  }

  protected function getPluginCollection() {
    if (!isset($this->pluginCollection)) {
      $this->pluginCollection = new DefaultSingleLazyPluginCollection(\Drupal::service('download_request_action.manager'), $this->plugin, $this->settings, $this->id());
    }
    return $this->pluginCollection;
  }

  public function getPlugin() {
    if (!$this->plugin) {
      return FALSE;
    }
    return $this->getPluginCollection()->get($this->plugin)
    ->setContextValue('download_request_action', $this);
  }

  public function setPlugin($plugin) {
    $this->plugin = $plugin;
  }

  public function getDownloadRequestType() {
    return $this->download_request_type;
  }

  public function setDownloadRequestType($type) {
    $this->download_request_type = $type;
  }

  public function getTransitionId() {
    return $this->transition_id;
  }

  public function setTransitionId($transition_id) {
    $this->transition_id = $transition_id;
  }

  protected function urlRouteParameters($rel) {
    $uri_route_parameters = parent::urlRouteParameters($rel);
    $uri_route_parameters['download_request_type'] = $this->getDownloadRequestType();

    return $uri_route_parameters;

  }
}
