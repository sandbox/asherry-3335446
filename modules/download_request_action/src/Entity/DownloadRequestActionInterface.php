<?php

namespace Drupal\download_request_action\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Download request email action entities.
 */
interface DownloadRequestActionInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
