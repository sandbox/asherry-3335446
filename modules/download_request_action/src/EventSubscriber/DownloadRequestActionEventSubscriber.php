<?php

namespace Drupal\download_request_action\EventSubscriber;

use Drupal\state_machine\Event\WorkflowTransitionEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Response to a workflow state transition.
 */
class DownloadRequestActionEventSubscriber implements EventSubscriberInterface {
  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new DefaultSubscriber object.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    // download_request is the group
    // transition id like "approved"
    // phase like 'pre_transition' || 'post_transition'
    $events['download_request.pre_transition'] = ['downloadRequestPreTransition'];

    return $events;
  }

  /**
   * This method is called when the download_request.pre_transition is dispatched.
   *
   * @param WorkflowTransitionEvent $event
   *   The dispatched event.
   */
  public function downloadRequestPreTransition(WorkflowTransitionEvent $event) {
    // TODO: Use the download_request_action.manager service, and add this hook.
    // This is currently not used.
  }

}
