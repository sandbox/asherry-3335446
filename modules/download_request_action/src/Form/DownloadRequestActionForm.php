<?php

namespace Drupal\download_request_action\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\Core\Plugin\PluginFormFactoryInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\download_request_action\Plugin\DownloadRequestActionInterface;
use Drupal\download_request_action\DownloadRequestActionManager;
use Drupal\entity\Form\EntityDuplicateFormTrait;
use Drupal\state_machine\WorkflowManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use function mb_strtolower;

/**
 * Class DownloadRequestActionForm.
 */
class DownloadRequestActionForm extends EntityForm {

  use EntityDuplicateFormTrait;

  /**
   * @var DownloadRequestActionManager
   */
  protected $downloadRequestActionManager;

  /**
   * @var PluginFormFactoryInterface
   */
  protected $pluginFormFactory;

  /**
   * @var WorkflowManagerInterface
   */
  protected $workflowManager;

  /**
   * DownloadRequestActionForm constructor.
   *
   * @param DownloadRequestActionManager $download_request_action_manager
   *   The download request action plugin manager.
   * @param PluginFormFactoryInterface $plugin_form_factory
   *   The plugin form factory service.
   * @param WorkflowManagerInterface $workflow_manager
   *   The workflow manager.
   */
  public function __construct(DownloadRequestActionManager $download_request_action_manager, PluginFormFactoryInterface $plugin_form_factory, WorkflowManagerInterface $workflow_manager) {
    $this->downloadRequestActionManager = $download_request_action_manager;
    $this->pluginFormFactory = $plugin_form_factory;
    $this->workflowManager = $workflow_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('download_request_action.manager'),
      $container->get('plugin_form.factory'),
      $container->get('plugin.manager.workflow')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $download_request_email_action = $this->entity;

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $download_request_email_action->label(),
      '#description' => $this->t("Label for the Download request email action."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $download_request_email_action->id(),
      '#machine_name' => [
        'exists' => '\Drupal\download_request_action\Entity\DownloadRequestAction::load',
      ],
      '#disabled' => !$download_request_email_action->isNew(),
    ];

    $form['status'] = [
      '#title' => $this->t('Status'),
      '#type' => 'checkbox',
      '#default_value' => $download_request_email_action->status(),
    ];
    $form['transition_id'] = [
      '#title' => $this->t('Transition'),
      '#type' => 'select',
      '#default_value' => $download_request_email_action->getTransitionId(),
      '#options' => ['download_request_entity_create' => $this->t('Download Request: Entity create')] + $this->getWorkflowTransitionOptions(),
      '#description' => $this->t('Choose the transition id which will trigger the current action.'),
    ];

    $form['plugin'] = [
      '#title' => $this->t('Action type'),
      '#type' => 'select',
      // TODO: This should pull in plugin types automatically.
      '#options' => ['download_request_email_action' => 'Email'],
      '#default_value' => $download_request_email_action->getPlugin() ? $download_request_email_action->getPlugin()->getPluginId() : 'download_request_email_action',
    ];

    // TODO: Potentially do an ajax refresh based on selected of plugin.
    $plugin_id = 'download_request_email_action';

    $form['settings'] = ['#tree' => TRUE];

    $subform_state = SubformState::createForSubform($form['settings'], $form, $form_state);
    $plugin = $this->downloadRequestActionManager->createInstance($plugin_id, $this->entity->get('settings'));

    $form['settings'] = $this->getPluginForm($plugin)->buildConfigurationForm($form['settings'], $subform_state);

    return $form;
  }

  protected function getWorkflowTransitionOptions() {
    $options = [];
    // TODO: Get the current workflow from the current DownloadRequest entity.
    $workflow = $this->workflowManager->createInstance('request_form');
    foreach ($workflow->getTransitions() as $id => $transition) {
      $options["download_request_item_transition_$id"] = $this->t('Download Request Item: Transition @label', [
        '@label' => mb_strtolower($transition->getLabel()),
      ]);
    }
    return $options;
  }

  protected function getPluginForm(DownloadRequestActionInterface $plugin) {
    return $this->pluginFormFactory->createInstance($plugin, 'configure');
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    $plugin = $this->entity->getPlugin();
    $this->getPluginForm($plugin)->validateConfigurationForm($form['settings'], SubformState::createForSubform($form['settings'], $form, $form_state));
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $entity = $this->entity;
    // The Block Entity form puts all block plugin form elements in the
    // settings form element, so just pass that to the block for submission.
    $sub_form_state = SubformState::createForSubform($form['settings'], $form, $form_state);
    // Call the plugin submit handler.
    $block = $entity->getPlugin();
    $this->getPluginForm($block)->submitConfigurationForm($form, $sub_form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $download_request_email_action = $this->entity;
    $status = $download_request_email_action->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Download request email action.', [
          '%label' => $download_request_email_action->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Download request email action.', [
          '%label' => $download_request_email_action->label(),
        ]));
    }

    $form_state->setRedirectUrl($download_request_email_action->toUrl('collection'));
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityFromRouteMatch(RouteMatchInterface $route_match, $entity_type_id) {
    $entity = parent::getEntityFromRouteMatch($route_match, $entity_type_id);
    if ($base_bundle_type = $route_match->getParameter('download_request_type')) {
      $entity->setDownloadRequestType($base_bundle_type->id());
    }
    return $entity;
  }
}
