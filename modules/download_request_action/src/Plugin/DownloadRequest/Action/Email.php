<?php

namespace Drupal\download_request_action\Plugin\DownloadRequest\Action;

use Drupal\Component\Render\PlainTextOutput;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageDefault;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\Markup;
use Drupal\download_request\Entity\DownloadRequest;
use Drupal\download_request\Entity\DownloadRequestItem;
use Drupal\download_request_action\Plugin\DownloadRequestActionBase;
use Drupal\download_request_action\Plugin\DownloadRequestActionInterface;
use Drupal\migrate\Plugin\migrate\process\Download;
use Drupal\token\Token;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @DownloadRequestAction (
 *   id = "download_request_email_action",
 *   admin_label = @Translation("Email"),
 *   context_definitions = {
 *     "download_request" = @ContextDefinition("entity:download_request", label = @Translation("Download Request"), required = FALSE),
 *     "download_request_item" = @ContextDefinition("entity:download_request_item", label = @Translation("Download Request Item"), required = FALSE),
 *     "download_request_action" = @ContextDefinition("entity:download_request_action", label = @Translation("Download Request Action"))
 *   }
 * )
 */
class Email extends DownloadRequestActionBase implements DownloadRequestActionInterface, ContainerFactoryPluginInterface {

  /**
   * @var MailManagerInterface
   */
  protected $mailManager;

  /**
   * @var LanguageDefault
   */
  protected $languageDefault;

  /**
   * @var ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * @var Token
   */
  protected $token;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MailManagerInterface $mail_manager, LanguageDefault $language_default, ConfigFactoryInterface $config_factory, Token $token) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->mailManager = $mail_manager;
    $this->languageDefault = $language_default;
    $this->configFactory = $config_factory;
    $this->token = $token;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.mail'),
      $container->get('language.default'),
      $container->get('config.factory'),
      $container->get('token')
    );
  }

  public function actionForm(array $form, FormStateInterface $form_state) {
    $form = parent::actionForm($form, $form_state);

    $settings = $this->configuration['mail'] ?? [];

    $form['mail'] = [
      '#title' => $this->t('Mail'),
      '#type' => 'details',
      '#open' => TRUE,
    ];

    $form['mail']['subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subject'),
      '#default_value' => $settings['subject'] ?? ''
    ];

    $form['mail']['recipients'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Recipients'),
      '#description' => $this->t('Comma separated list of email addresses.'),
      '#default_value' => $settings['recipients'] ?? ''
    ];

    $form['mail']['body'] = [
      '#type' => 'text_format',
      '#format' => 'full_html',
      '#allowed_formats' => ['full_html'],
      '#title' => $this->t('Body'),
      '#default_value' => $settings['body']['value'] ?? ''
    ];

    $form['mail']['token'] = [
      '#theme' => 'token_tree_link',
      '#token_types' => ['download_request_item', 'user', 'download_request'],
    ];

    return $form;
  }

  public function actionSubmit($form, FormStateInterface $form_state) {
    parent::actionSubmit($form, $form_state);
    $this->configuration['mail'] = $form_state->get('mail');
  }

  protected function getMailRecipients() {
    $settings = $this->getMailSettings();
    if (empty($settings['recipients'])) {
      return [];
    }
    $recipients = explode(',', $settings['recipients']);
    array_walk($recipients, 'trim');
    return $recipients;
  }

  protected function getMailBody() {
    $settings = $this->getMailSettings();
    return $settings['body']['value'] ?? NULL;
  }

  protected function getMailSubject() {
    $settings = $this->getMailSettings();
    return $settings['subject'] ?? NULL;
  }

  /**
   * Gets settings for the download_request_action config entity from context.
   *
   * @return array|mixed
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  protected function getMailSettings() {
    $download_request_action = $this->getContextValue('download_request_action');
    $settings = $download_request_action->get('settings') ?? [];
    return $settings['mail'] ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function execute() {
    if (!$this->getDownloadRequest() && !$this->getDownloadRequestItem()) {
      return;
    }
    $params = [
      'plugin' => $this,
    ];

    foreach ($this->getMailRecipients() as $recipient) {
      $langcode = $this->languageDefault->get()->getId();

      $site_mail = $this->configFactory->get('system.site')->get('mail_notification');
      // If the custom site notification email has not been set, we use the site
      // default for this.
      if (empty($site_mail)) {
        $site_mail = $this->configFactory->get('system.site')->get('mail');
      }
      if (empty($site_mail)) {
        $site_mail = ini_get('sendmail_from');
      }

      $this->mailManager->mail('download_request_action', 'default', $recipient, $langcode, $params, $site_mail);
    }
  }

  private function getDownloadRequestItem() {
    if (!$download_request_item = $this->getContextValue('download_request_item')) {
      return FALSE;
    }
    if (!is_a($download_request_item, DownloadRequestItem::class)) {
      $download_request_item = DownloadRequestItem::load($download_request_item);
    }
    return $download_request_item;
  }

  private function getDownloadRequest() {
    if (!$download_request = $this->getContextValue('download_request')) {
      return FALSE;
    }
    if (!is_a($download_request, DownloadRequest::class)) {
      $download_request = DownloadRequest::load($download_request);
    }
    return $download_request;
  }

  /**
   * Called from hook_mail().
   *
   * @see download_request_action_mail()
   */
  public function mail($key, &$message, $params) {
    $langcode = $message['langcode'];
    $variables = [];
    if ($download_request_item = $this->getDownloadRequestItem()) {
      $variables['download_request_item'] = $download_request_item;
      $variables['download_request'] = $download_request_item->getParentEntity();
    }
    if ($download_request = $this->getDownloadRequest()) {
      $variables['download_request'] = $download_request;
    }
    if (!empty($variables['download_request'])) {
      $variables['user'] = $variables['download_request']->getOwner();
    }
//    $language = $language_manager->getLanguage($params['account']->getPreferredLangcode());
//    $original_language = $language_manager->getConfigOverrideLanguage();
//    $language_manager->setConfigOverrideLanguage($language);

//    $token_options = ['langcode' => $langcode, 'callback' => 'user_mail_tokens', 'clear' => TRUE];
    $token_options = ['langcode' => $langcode];
    $message['subject'] .= PlainTextOutput::renderFromHtml($this->token->replace($this->getMailSubject(), $variables, $token_options));
    $markup = $this->token->replace($this->getMailBody(), $variables, $token_options);
    $message['body'][] = Markup::create($markup);

//    $language_manager->setConfigOverrideLanguage($original_language);
  }
}
