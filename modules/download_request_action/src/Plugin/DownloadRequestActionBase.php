<?php

namespace Drupal\download_request_action\Plugin;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContextAwarePluginBase;
use Drupal\Core\Plugin\ContextAwarePluginTrait;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Plugin\PluginWithFormsTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Base class for Download request action type plugins.
 */
abstract class DownloadRequestActionBase extends PluginBase implements DownloadRequestActionInterface {

  use PluginWithFormsTrait;
  use StringTranslationTrait;
  use ContextAwarePluginTrait;

  /**
   * Return the plugin label.
   *
   * @return mixed|string
   */
  public function label() {
    if (!empty($this->configuration['label'])) {
      return $this->configuration['label'];
    }

    $definition = $this->getPluginDefinition();
    // Cast the admin label to a string since it is an object.
    // @see \Drupal\Core\StringTranslation\TranslatableMarkup
    return (string) $definition['admin_label'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form += $this->actionForm($form, $form_state);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    // Remove the admin_label form item element value so it will not persist.
    // ?
    $form_state->unsetValue('admin_label');

    $this->actionValidate($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    // Process the block's submission handling if no errors occurred only.
    if (!$form_state->getErrors()) {
      $this->configuration['label'] = $form_state->getValue('label');
      $this->configuration['label_display'] = $form_state->getValue('label_display');
      $this->configuration['provider'] = $form_state->getValue('provider');
      $this->actionSubmit($form, $form_state);
    }
  }

  /**
   * This is the form that the plugin implementations override, like ::blockForm
   * - the entity form should call ::form
   * - the use of PluginFormInterface allows us to return THIS class as the form class
   * - then PluginFormFactory calls it automatically
   */
  public function actionForm(array $form, FormStateInterface $form_state) {
    return $form;
  }

  public function actionValidate($form, FormStateInterface $form_state) {}

  public function actionSubmit($form, FormStateInterface $form_state) {}



}
