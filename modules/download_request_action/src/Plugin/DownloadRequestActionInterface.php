<?php

namespace Drupal\download_request_action\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\Plugin\PluginWithFormsInterface;

/**
 * Defines an interface for Download request action type plugins.
 */
interface DownloadRequestActionInterface extends PluginInspectionInterface, PluginFormInterface, PluginWithFormsInterface {

  /**
   * Main callback to run the action.
   */
  public function execute();
}
