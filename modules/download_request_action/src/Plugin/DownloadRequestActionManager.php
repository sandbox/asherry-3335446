<?php

namespace Drupal\download_request_action\Plugin;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Provides the Download request action type plugin manager.
 *
 * @deprecated
 *   Moved to \Drupal\download_request_action\DownloadRequestActionManager
 */
class DownloadRequestActionManager extends DefaultPluginManager {


  /**
   * Constructs a new DownloadRequestActionManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/DownloadRequest/Action', $namespaces, $module_handler, 'Drupal\download_request_action\Plugin\DownloadRequestActionInterface', 'Drupal\download_request_action\Annotation\DownloadRequestAction');

    $this->alterInfo('download_request_action_download_request_action_type_info');
    $this->setCacheBackend($cache_backend, 'download_request_action_download_request_action_type_plugins');
  }
}
