<?php

declare(strict_types=1);

namespace Drupal\download_request_form\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\download_request_form\Entity\DownloadRequestForm;

/**
 * Define the download request form access.
 */
class DownloadRequestFormAccess implements AccessInterface {

  /**
   * Check the download request form access.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *
   * @return \Drupal\Core\Access\AccessResult|\Drupal\Core\Access\AccessResultForbidden|\Drupal\Core\Access\AccessResultReasonInterface
   */
  public function access(AccountInterface $account, DownloadRequestForm $download_request_form) {
    return AccessResult::allowedIfHasPermission(
      $account,
      $download_request_form->getViewPermission()
    );
  }
}
