<?php

declare(strict_types=1);

namespace Drupal\download_request_form\Contract;

interface DownloadRequestFormInterface {

  /**
   * Determine if an entity exists.
   *
   * @param string $id
   *   The entity identifier.
   *
   * @return bool
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function exists(string $id): bool;
}
