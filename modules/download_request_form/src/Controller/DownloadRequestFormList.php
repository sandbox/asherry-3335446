<?php

declare(strict_types=1);

namespace Drupal\download_request_form\Controller;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;

/**
 * Define the download request route list.
 */
class DownloadRequestFormList extends EntityListBuilder {

  /**
   * {@inheritDoc}
   */
  public function buildHeader() {
    return [
      'label' => $this->t('Form Label')
    ] + parent::buildHeader();
  }

  /**
   * {@inheritDoc}
   */
  public function buildRow(EntityInterface $entity) {
    return [
      'label' => $entity->label()
    ] + parent::buildRow($entity);
  }
}
