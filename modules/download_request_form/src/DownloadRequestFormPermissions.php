<?php

declare(strict_types=1);

namespace Drupal\download_request_form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;

/**
 * Define the download request form permissions.
 */
class DownloadRequestFormPermissions implements ContainerInjectionInterface {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructor for the download request form permissions.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * Build permissions for each download request form entity.
   *
   * @return array
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function permissions(): array {
    $permissions = [];

    /** @var \Drupal\download_request_form\Entity\DownloadRequestForm $entity */
    foreach ($this->entities() as $entity) {
      if ($permission = $entity->getViewPermission()) {
        $permissions[$permission] = [
          'title' => "View the {$entity->label()} download request form"
        ];
      }
    }

    return $permissions;
  }

  /**
   * Get all download request form entities.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function entities(): array {
    return $this->entityTypeManager
      ->getStorage('download_request_form')->loadMultiple();
  }
}
