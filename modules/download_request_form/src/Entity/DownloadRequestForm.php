<?php

declare(strict_types=1);

namespace Drupal\download_request_form\Entity;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\Annotation\ConfigEntityType;
use Drupal\download_request_form\Contract\DownloadRequestFormInterface;

/**
 * Define the download request form entity configuration.
 *
 * @ConfigEntityType(
 *   id = "download_request_form",
 *   label = @Translation("Download Request Form"),
 *   label_plural = @Translation("Download Request Forms"),
 *   label_singular = @Translation("Download Request Form"),
 *   label_collection = @Translation("Download Request Form"),
 *   admin_permission = "administer download request route form",
 *   config_prefix = "form",
 *   entity_keys = {
 *     "id" = "name",
 *     "label" = "label"
 *   },
 *   config_export = {
 *     "name",
 *     "label",
 *     "item_fields",
 *     "item_bundle_type",
 *     "item_display_limit",
 *     "redirect_uri",
 *     "request_bundle_type",
 *     "item_filter_form_mode"
 *   },
 *   handlers = {
 *     "form" = {
 *       "add" = "\Drupal\download_request_form\Form\DownloadRequestDefaultForm",
 *       "edit" = "\Drupal\download_request_form\Form\DownloadRequestDefaultForm",
 *       "delete" = "\Drupal\download_request_form\Form\DownloadRequestDeleteForm",
 *       "default" = "\Drupal\download_request_form\Form\DownloadRequestDefaultForm"
 *     },
 *     "route_provider" = {
 *       "html" = "\Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider"
 *     },
 *     "list_builder" = "\Drupal\download_request_form\Controller\DownloadRequestFormList"
 *   },
 *   links = {
 *     "collection" = "/admin/config/system/download-request-form",
 *     "add-form" = "/admin/config/system/download-request-form/add",
 *     "edit-form" = "/admin/config/system/download-request-form/{download_request_form}",
 *     "delete-form" = "/admin/config/system/download-request-form/{download_request_form}/delete"
 *   }
 * )
 */
class DownloadRequestForm extends ConfigEntityBase implements DownloadRequestFormInterface {

  /**
   * @var string
   */
  protected $name;

  /**
   * @var array
   */
  protected $item_fields = [];

  /**
   * @var string
   */
  protected $item_bundle_type;

  /**
   * @var integer
   */
  protected $item_display_limit;

  /**
   * @var string
   */
  protected $redirect_uri;

  /**
   * @var string
   */
  protected $request_bundle_type;

  /**
   * @var string
   */
  protected $item_filter_form_mode;

  /**
   * {@inheritDoc}
   */
  public function id() {
    return $this->name;
  }

  /**
   * Get download item fields.
   *
   * @return array
   */
  public function itemFields(): array {
    return $this->item_fields;
  }

  /**
   * Get download item bundle type.
   *
   * @return string
   */
  public function itemBundleType(): string {
    return $this->item_bundle_type ?? '';
  }

  /**
   * Get download item filter form mode.
   *
   * @return string
   */
  public function itemFilterFormMode(): string {
    return $this->item_filter_form_mode ?? 'default';
  }

  /**
   * Get download request form bundle type.
   *
   * @return string
   */
  public function requestBundleType(): string {
    return $this->request_bundle_type ?? '';
  }

  /**
   * Get download item display limit.
   *
   * @return int
   */
  public function itemDisplayLimit(): int {
    return (int) $this->item_display_limit ?: 15;
  }

  public function getRedirectUri(): ?string {
    return $this->redirect_uri;
  }

  public function setRedirectUri($uri) {
    $this->redirect_uri = $uri;
  }

  /**
   * Get the download request form view permission.
   *
   * @return string
   */
  public function getViewPermission(): string {
    return "view the {$this->id()} download request form";
  }

  /**
   * {@inheritDoc}
   */
  public function exists(string $name): bool {
    return (bool) $this->entityStorage()
        ->getQuery()
        ->accessCheck(TRUE)
        ->condition('name', $name)
        ->count()
        ->execute() === 1;
  }

  /**
   * Show the filter fields.
   *
   * @return array
   *   An array of download item fields.
   */
  public function showFilterFields(): array {
    return $this->getItemFields('show_in_filter');
  }

  /**
   * Show the listing fields.
   *
   * @return array
   *   An array of download item fields.
   */
  public function showListingFields(): array {
    return $this->getItemFields('show_in_listing');
  }

  /**
   * Show the summary fields.
   *
   * @return array
   *   An array of download item fields.
   */
  public function showSummaryFields(): array {
    return $this->getItemFields('show_in_summary');
  }

  /**
   * Get download item fields filtered by value key.
   *
   * @param string $value_key
   *   The download item field key.
   *
   * @return array
   *   An array of download item fields.*
   */
  protected function getItemFields(string $value_key): array {
    return array_filter($this->itemFields(), function ($value) use ($value_key) {
      return isset($value[$value_key]) && $value[$value_key];
    });
  }

  /**
   * The entity storage instance.
   *
   * @return \Drupal\Core\Entity\EntityStorageInterface
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function entityStorage(): EntityStorageInterface {
    return $this->entityTypeManager()->getStorage($this->entityTypeId);
  }
}
