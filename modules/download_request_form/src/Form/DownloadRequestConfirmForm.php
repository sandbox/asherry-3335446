<?php

declare(strict_types=1);

namespace Drupal\download_request_form\Form;

use Drupal\Core\Url;
use Drupal\field\FieldConfigInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\download_request\Entity\DownloadRequest;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Define the download request selection confirm form.
 */
class DownloadRequestConfirmForm extends DownloadRequestFormBase {

  /**
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityDisplayRepository;

  /**
   * The download request confirm form constructor.
   *
   * @param \Drupal\Core\Render\RendererInterface $renderer
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository
   */
  public function __construct(
    RendererInterface $renderer,
    EntityTypeManagerInterface $entity_type_manager,
    EntityFieldManagerInterface $entity_field_manager,
    EntityDisplayRepositoryInterface $entity_display_repository
  ) {
    parent::__construct($renderer, $entity_type_manager);
    $this->entityFieldManager = $entity_field_manager;
    $this->entityDisplayRepository = $entity_display_repository;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('renderer'),
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('entity_display.repository')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId(): string {
    return 'download_request_confirm_form';
  }

  /**
   * Get the current referenced DownloadRequestForm entity.
   *
   * @return \Drupal\download_request_form\Entity\DownloadRequestForm
   */
  public function getDownloadRequestForm() {
    return $this->downloadRequestForm;
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['#prefix'] = '<div id="download-request-form-confirmation">';
    $form['#suffix'] = '</div>';

    $form += $this->form($form, $form_state);

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
      '#value' => $this->t('Submit Request')
    ];
    $form['actions']['reset'] = [
      '#type' => 'submit',
      '#value' => $this->t('Reset'),
      '#submit' => [
        [$this, 'submitRequestConfirmReset']
      ],
      '#limit_validation_errors' => [],
    ];

    return $form;
  }

  /**
   * The download request confirmation operation submit.
   *
   * @param array $form
   *   An array of form elements.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state instance.
   */
  public function downloadRequestConfirmOpSubmit(
    array $form,
    FormStateInterface $form_state
  ): void {
    $button = $form_state->getTriggeringElement();

    if (isset($button['#op'])) {
      switch ($button['#op']) {
        case 'remove':
          if ($entity_id = $button['#entity_id']) {
            $removed_ids = $form_state->get('request_removed_ids') ?? [];

            if (!in_array($entity_id, $removed_ids)) {
              $removed_ids[] = $entity_id;
              $form_state->set('request_removed_ids', $removed_ids);
            }
          }
          break;
      }
    }
    $form_state->setRebuild();
  }

  /**
   * Download request confirmation operation ajax submit.
   *
   * @param array $form
   *   An array of form elements.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state instance.
   *
   * @return array
   *   An array of form elements.
   */
  public function downloadRequestOperationAjaxSubmit(
    array $form,
    FormStateInterface $form_state
  ): array {
    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $confirm_ids = array_filter($form_state->getValue('confirmed_ids'));

    if (empty($confirm_ids)) {
      $form_state->setError(
        $form['confirmed_ids'],
        $this->t('At least one download request needs to be confirmed.')
      );
    }
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    try {
      $this->createDownloadRequest($form_state)->save();

      $redirect_uri = $this->downloadRequestFormEntity()->getRedirectUri();
      if (!empty($redirect_uri)) {
        $redirect_url = Url::fromUserInput($redirect_uri);
        $form_state->setRedirectUrl($redirect_url);
      }
      else {
        $form_state->setRedirect(
          'download_request_form.request.listing_form',
          ['download_request_form' => $this->downloadRequestFormEntity()->id()]
        );
      }
    } catch (\Exception $exception) {
      watchdog_exception('download_request_form', $exception);
    }
  }

  /**
   * Submit request confirmation reset.
   *
   * @param array $form
   *   An array of form elements.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state instance.
   *
   * @throws \Exception
   */
  public function submitRequestConfirmReset(
    array $form,
    FormStateInterface $form_state
  ): void {
    $form_state->setRedirect(
      'download_request_form.request.listing_form',
      ['download_request_form' => $this->downloadRequestFormEntity()->id()]
    );
  }

  /**
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return \Drupal\Core\Entity\EntityBase|\Drupal\Core\Entity\EntityInterface
   * @throws \Exception
   */
  protected function createDownloadRequest(FormStateInterface $form_state) {
    return DownloadRequest::createDownloadRequestWithItems(
      $this->getDownloadRequestBundleType(),
      $form_state->getValue('request_fields'),
      $form_state->getValue('confirmed_ids')
    );
  }

  /**
   * Define the download request form elements.
   *
   * @param array $form
   *   An array of form elements.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state instance.
   *
   * @return array
   *   An array of the form elements.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Exception
   */
  protected function form(array $form, FormStateInterface $form_state): array {
    $request_ids = $this->getDownloadItemRequestIds($form_state);

    $form['confirmed_ids'] = [
      '#type' => 'hidden',
      '#value' => $request_ids
    ];
    $form['request_summary'] = [
      '#type' => 'table',
      '#header' => $this->buildSummaryTableHeaders(),
      '#empty' => $this->t('There are no download items requested.'),
    ] + $this->buildSummaryTableRows($request_ids);

    $form['request_fields'] = [
      '#type' => 'container',
      '#tree' => TRUE,
    ];
    $form['request_fields'] += $this->buildEntityFormDisplay(
      'download_request',
      $this->getDownloadRequestBundleType(),
      $form_state,
      ['request_fields']
    );

    return $form;
  }

  /**
   * Get download item request identifies.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state instance.
   *
   * @return array
   *   An array of download item entity ids.
   */
  protected function getDownloadItemRequestIds(
    FormStateInterface $form_state
  ): array {
    return array_diff(
      $this->getRequest()->query->get('request_ids', []),
      $form_state->get('request_removed_ids') ?? []
    );
  }

  /**
   * Build the download item summary table headers.
   *
   * @return array[]
   *   An array of the summary table headers.
   *
   * @throws \Exception
   */
  protected function buildSummaryTableHeaders(): array {
    $request_form = $this->downloadRequestFormEntity();

    $headers = $this->buildEntityTableHeaders(
      $request_form->showSummaryFields()
    );
    $headers['operations'] = $this->t('Operations');

    return $headers;
  }

  /**
   * Build the download item summary table rows.
   *
   * @param array $request_ids
   *   An array of the download item request ids.
   *
   * @return array
   *   An array of download item rows.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Exception
   */
  protected function buildSummaryTableRows(
    array $request_ids
  ): array {
    return $this->buildEntityTableRows(
      $this->getDownloadItemStorage()->loadMultiple($request_ids),
      $this->downloadRequestFormEntity()->showSummaryFields(),
      'markup'
    );
  }

  /**
   * {@inheritDoc}
   */
  protected function buildEntityTableOperation(
    ContentEntityInterface $entity
  ): array {
    $operations = [];

    $operations['remove'] = [
      '#op' => 'remove',
      '#type' => 'submit',
      '#value' => $this->t('Remove'),
      '#entity_id' => $entity->id(),
      '#submit' => [
        [$this, 'downloadRequestConfirmOpSubmit']
      ],
      '#limit_validation_errors' => [],
      '#ajax' => [
        'event' => 'click',
        'method' => 'replace',
        'wrapper' => 'download-request-form-confirmation',
        'callback' => [$this, 'downloadRequestOperationAjaxSubmit']
      ],
      '#name' => "download-request-confirmation-remove-{$entity->id()}",
    ];

    return ['operations' => $operations];
  }

  /**
   * Build the entity form display.
   *
   * @param string $entity_type_id
   *   The entity type identifier.
   * @param string $bundle
   *   The entity bundle type.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state instance.
   * @param array $parents
   *   An array of the parents.
   *
   * @return array
   *   An array of the entity form fields.
   */
  protected function buildEntityFormDisplay(
    string $entity_type_id,
    string $bundle,
    FormStateInterface $form_state,
    $parents = []
  ): array {
    $fields = [];

    $form = ['#parents' => $parents];
    $entity = DownloadRequest::create(['type' => $bundle]);

    $display = $this->entityDisplayRepository
      ->getFormDisplay($entity_type_id, $bundle);
    $definitions = $this->entityFieldManager
      ->getFieldDefinitions($entity_type_id, $bundle);

    foreach ($display->getComponents() as $name => $options) {
      if (!isset($definitions[$name])
        || !$definitions[$name] instanceof FieldConfigInterface) {
        continue;
      }

      /** @var \Drupal\Core\Field\WidgetBase $widget */
      if ($widget = $display->getRenderer($name)) {
        $items = $entity->get($name);
        $fields[$name] = $widget->form($items, $form, $form_state);
        $fields[$name]['#weight'] = $options['weight'];
      }
    }

    return $fields;
  }

}
