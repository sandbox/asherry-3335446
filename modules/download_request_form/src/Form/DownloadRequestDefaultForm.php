<?php

declare(strict_types=1);

namespace Drupal\download_request_form\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\field\FieldConfigInterface;
use Drupal\Component\Utility\SortArray;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\download_request_form\FormStateInputTrait;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Define the download request route default form.
 */
class DownloadRequestDefaultForm extends EntityForm {

  use FormStateInputTrait;

  /**
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityDisplayRepository;

  /**
   * Constructor for the download request form default form.
   *
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository
   */
  public function __construct(
    EntityFieldManagerInterface $entity_field_manager,
    EntityTypeBundleInfoInterface $entity_type_bundle_info,
    EntityDisplayRepositoryInterface $entity_display_repository
  ) {
    $this->entityFieldManager = $entity_field_manager;
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
    $this->entityDisplayRepository = $entity_display_repository;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_field.manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('entity_display.repository')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\download_request_form\Entity\DownloadRequestForm $entity */
    $entity = $this->entity;

    $form = parent::form($form, $form_state);

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Form Label'),
      '#maxlength' => 255,
      '#default_value' => $entity->label(),
      '#description' => $this->t('Input the download request form label.'),
      '#required' => TRUE,
    ];
    $form['name'] = [
      '#type' => 'machine_name',
      '#machine_name' => [
        'exists' => [$entity, 'exists'],
      ],
      '#disabled' => !$entity->isNew(),
      '#default_value' => $entity->id(),
    ];
    $form['request_form'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Download Request Form'),
      '#prefix' => '<div id="download-request-form-configuration">',
      '#suffix' => '</div>',
      '#tree' => FALSE,
    ];
    $form['request_form']['request_bundle_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Download Request Bundle'),
      '#description' => $this->t('Select the download request bundle type. </br>
        <strong>Note:</strong> This request bundle type is used to create the
        download request entity.'),
      '#options' => $this->getDownloadRequestBundleOptions(),
      '#required' => TRUE,
      '#empty_option' => $this->t('- Select -'),
      '#default_value' => $entity->requestBundleType(),
    ];

    $item_bundle_type = $this->getFormStateInputValue(
      'item_bundle_type',
      $form_state,
      $entity->itemBundleType()
    );

    $form['request_form']['item_bundle_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Download Item Bundle'),
      '#description' => $this->t('Select the download item bundle type.'),
      '#options' => $this->getDownloadItemBundleOptions(),
      '#required' => TRUE,
      '#empty_option' => $this->t('- Select -'),
      '#default_value' => $item_bundle_type,
      '#ajax' => [
        'event' => 'change',
        'method' => 'replace',
        'wrapper' => 'download-request-form-configuration',
        'callback' => [$this, 'downloadRequestAjaxCallback']
      ]
    ];

    if (isset($item_bundle_type) && !empty($item_bundle_type)) {
      $form['request_form']['item_filter_form_mode'] = [
        '#type' => 'select',
        '#title' => $this->t('Download Item Filter Form Mode'),
        '#description' => $this->t(
          'Select the form mode to use for the filter input.'
        ),
        '#options' => $this->getDownloadItemFormModes($item_bundle_type),
        '#required' => TRUE,
        '#default_value' => $entity->itemFilterFormMode(),
      ];
      $form['request_form']['item_display_limit'] = [
        '#type' => 'number',
        '#title' => $this->t('Download Item Listing Display Limit'),
        '#description' => $this->t(
          'Input the number of items to show prior to paginating.'
        ),
        '#default_value' => $entity->itemDisplayLimit(),
        '#required' => TRUE,
        '#min' => 1,
      ];
      $form['request_form']['redirect_uri'] = [
        // TODO: Add a validation for a proper url and help text about the
        // required '/'
        '#type' => 'textfield',
        '#title' => $this->t('Redirect URL'),
        '#default_value' => $entity->getRedirectUri(),
      ];
      $form['request_form']['item_fields'] = [
        '#type' => 'table',
        '#tree' => TRUE,
        '#header' => [
          'name' => $this->t('Field Name'),
          'label' => $this->t('Field Label'),
          'show_in_listing' => $this->t('Show In Listing'),
          'show_in_summary' => $this->t('Show In Summary'),
          'show_in_filter' => $this->t('Show In Filter'),
          'weight' => $this->t('Weight')
        ],
        '#tabledrag' => [
          [
            'action' => 'order',
            'relationship' => 'sibling',
            'group' => 'table-sort-weight',
          ]
        ],
        '#empty' => $this->t('There are no fields for this bundle to configure.')
      ];
      $fields = [];
      $item_fields = $entity->itemFields();

      foreach ($this->getDownloadItemFields($item_bundle_type) as $field_name => $field) {
        $field_name = $field->getName();
        $fields[$field_name]['#attributes']['class'][] = 'draggable';
        $fields[$field_name]['name'] = [
          '#plain_text' => $field_name,
        ];
        $fields[$field_name]['label'] = [
          '#type' => 'textfield',
          '#required' => TRUE,
          '#default_value' => $item_fields[$field_name]['label'] ?? $field->getLabel()
        ];
        $fields[$field_name]['show_in_listing'] = [
          '#type' => 'checkbox',
          '#default_value' => $item_fields[$field_name]['show_in_listing'] ?? FALSE
        ];
        $fields[$field_name]['show_in_summary'] = [
          '#type' => 'checkbox',
          '#default_value' => $item_fields[$field_name]['show_in_summary'] ?? FALSE,
        ];
        $fields[$field_name]['show_in_filter'] = [
          '#type' => 'checkbox',
          '#default_value' => $item_fields[$field_name]['show_in_filter'] ?? FALSE
        ];
        $fields[$field_name]['weight'] = [
          '#type' => 'weight',
          '#title' => $this->t('Weight'),
          '#title_display' => 'invisible',
          '#default_value' => $item_fields[$field_name]['weight'] ?? 0,
          '#attributes' => [
            'class' => [
              'table-sort-weight',
            ],
          ],
        ];
      }
      uasort($fields, [SortArray::class, 'sortByWeightElement']);

      $form['request_form']['item_fields'] += $fields;
    }

    return $form;
  }

  /**
   * Download request ajax callback.
   *
   * @param array $form
   *   An array of form elements.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The forms state instance.
   *
   * @return array
   *   An array of form elements.
   */
  public function downloadRequestAjaxCallback(
    array $form,
    FormStateInterface $form_state
  ): array {
    return $form['request_form'];
  }

  /**
   * {@inheritDoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $status = parent::save($form, $form_state);

    /** @var \Drupal\download_request_form\Entity\DownloadRequestForm $entity */
    $entity = $this->entity;

    $this->messenger()->addStatus(
      $this->t("You've successfully @action the @label.", [
        '@label' => $entity->label(),
        '@action' => SAVED_NEW === $status ? 'added' : 'updated'
      ])
    );
    $form_state->setRedirectUrl($entity->toUrl('collection'));

    return $status;
  }

  protected function getDownloadItemFormModes(string $bundle): array {
    return $this->entityDisplayRepository
      ->getFormModeOptionsByBundle('download_item', $bundle);
  }

  /**
   * Get the download item fields.
   *
   * @param string $bundle
   *   The download item bundle type.
   *
   * @return array
   *   An array of fields.
   */
  protected function getDownloadItemFields(string $bundle): array {
    $fields = [];
    $allowed_base_fields = ['name'];

    $definitions = $this->entityFieldManager
      ->getFieldDefinitions('download_item', $bundle);

    foreach ($definitions as $field_name => $definition) {
      if ((!$definition instanceof FieldConfigInterface
          || $definition->isComputed()) && !in_array($field_name, $allowed_base_fields)) {
        continue;
      }
      $fields[] = $definition;
    }

    return $fields;
  }

  /**
   * Get the download item bundle options.
   *
   * @return array
   *   An array of bundle options.
   */
  protected function getDownloadItemBundleOptions(): array {
    return $this->getBundleOptions('download_item');
  }

  /**
   * Get the download request bundle options.
   *
   * @return array
   *   An array of bundle options.
   */
  protected function getDownloadRequestBundleOptions(): array {
    return $this->getBundleOptions('download_request');
  }

  /**
   * Get bundle options by entity type.
   *
   * @param string $entity_type_id
   *   The entity type identifier.
   *
   * @return array
   *   An array of bundle options.
   */
  protected function getBundleOptions(string $entity_type_id) {
    $options = [];
    $bundles = $this->entityTypeBundleInfo->getBundleInfo($entity_type_id);

    foreach ($bundles as $name => $bundle) {
      if (!isset($bundle['label'])) {
        continue;
      }
      $options[$name] = $bundle['label'];
    }

    return $options;
  }
}
