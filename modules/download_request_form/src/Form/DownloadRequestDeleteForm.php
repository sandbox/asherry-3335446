<?php

declare(strict_types=1);

namespace Drupal\download_request_form\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityConfirmFormBase;

/**
 * Define the download request route delete form.
 */
class DownloadRequestDeleteForm extends EntityConfirmFormBase {

  /**
   * {@inheritDoc}
   */
  public function getQuestion() {
    return $this->t(
      'Are you sure you want to delete %label?',
      ['%label' => $this->entity->label()]
    );
  }

  /**
   * {@inheritDoc}
   */
  public function getCancelUrl() {
    return $this->entity->toUrl('collection');
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    /** @var \Drupal\download_request_form\Entity\DownloadRequestForm $entity */
    $entity = $this->entity;

    $this->messenger()->addStatus(
      $this->t("You've successfully deleted @label", [
        '@label' => $entity->label()
      ])
    );
    $entity->delete();
  }
}
