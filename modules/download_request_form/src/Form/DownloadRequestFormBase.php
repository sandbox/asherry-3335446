<?php

declare(strict_types=1);

namespace Drupal\download_request_form\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Render\Element;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\download_request_form\FormStateInputTrait;
use Drupal\download_request\Entity\DownloadItemInterface;
use Drupal\download_request_form\Entity\DownloadRequestForm;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\download_request_form\Contract\DownloadRequestFormInterface;

/**
 * Define the download request form base class.
 */
abstract class DownloadRequestFormBase extends FormBase {

  use FormStateInputTrait;

  /**
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\download_request_form\Entity\DownloadRequestForm
   */
  protected $downloadRequestForm;

  /**
   * The download request form base constructor.
   *
   * @param \Drupal\Core\Render\RendererInterface $renderer
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   */
  public function __construct(
    RendererInterface $renderer,
    EntityTypeManagerInterface $entity_type_manager
  ) {
    $this->renderer = $renderer;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('renderer'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Build entity table headers.
   *
   * @param array $fields
   *   An array of download request form fields
   *
   * @return array[]
   *   An array of table headers.
   */
  protected function buildEntityTableHeaders(array $fields): array {
    $headers = [];

    foreach ($fields as $field_name => $field) {
      if (!isset($field['label'])) {
        continue;
      }
      $headers[$field_name] = [
        'data' => $field['label'],
        'specifier' => $field_name
      ];
    }

    return $headers;
  }

  /**
   * Build the entities table rows.
   *
   * @param array $entities
   *   An array of download item entities.
   * @param array $fields
   *   An array of fields.
   *
   * @param string|null $render_type
   *
   * @return array
   *   An array of download item table rows.
   */
  protected function buildEntityTableRows(
    array $entities,
    array $fields,
    string $render_type = NULL
  ): array {
    $rows = [];

    /** @var \Drupal\download_request\Entity\DownloadItem $entity */
    foreach ($entities as $entity_id => $entity) {
      if (!$entity instanceof DownloadItemInterface) {
        continue;
      }
      $rows[$entity_id] = array_merge(
        $this->renderFields($entity, $fields, $render_type),
        $this->buildEntityTableOperation($entity)
      );
    }

    return $rows;
  }

  /**
   * Build the entity table operation.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity instance.
   *
   * @return array
   *   An array of the entity related operations.
   */
  protected function buildEntityTableOperation(
    ContentEntityInterface $entity
  ): array {
    return [];
  }

  /**
   * Render the fields for the given entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The content entity instance.
   * @param array $fields
   *   An array of the fields to render.
   * @param string|null $render_type
   *   Input the render type for the field.
   *
   * @return array
   *   An array of rendered field markup.
   */
  protected function renderFields(
    ContentEntityInterface $entity,
    array $fields,
    string $render_type = NULL
  ): array {
    $output = [];

    foreach (array_keys($fields) as $field_name) {
      if (!$entity->hasField($field_name)) {
        continue;
      }
      $elements = $entity->get($field_name)->view();

      foreach (Element::children($elements) as $delta) {
        if (!isset($elements[$delta])) {
          continue;
        }
        $item = $this->renderer->renderRoot($elements[$delta]);

        if ($render_type === 'markup') {
          $item = ['#markup' => $item];
        }

        $output[$field_name][$delta] = $item;
      }
    }

    return $output;
  }

  /**
   * Filter the element children.
   *
   * @param array $elements
   *   An array of elements.
   * @param array $exclude_keys
   *   An array of excluded keys.
   *
   * @return array
   *   An array of elements filtered.
   */
  protected function filterElementChildren(
    array $elements,
    array $exclude_keys
  ): array {
    return array_filter(Element::children($elements), function ($name) use ($exclude_keys) {
      return !in_array($name, $exclude_keys);
    });
  }

  /**
   * Get the download item display limit.
   *
   * @return int
   *   The download item display limit.
   *
   * @throws \Exception
   */
  protected function getDownloadItemDisplayLimit(): int {
    return $this->downloadRequestFormEntity()->itemDisplayLimit();
  }

  /**
   * Get the download item bundle type.
   *
   * @return string
   *   The download item bundle type.
   *
   * @throws \Exception
   */
  protected function getDownloadItemBundleType(): string {
    return $this->downloadRequestFormEntity()->itemBundleType();
  }

  /**
   * Get the download request bundle type.
   *
   * @return string
   *   The download request bundle type.
   *
   * @throws \Exception
   */
  protected function getDownloadRequestBundleType(): string {
    return $this->downloadRequestFormEntity()->requestBundleType();
  }

  /**
   * Get the download request form entity.
   *
   * @return \Drupal\download_request_form\Entity\DownloadRequestForm
   *   The download request form instance.
   *
   * @throws \Exception
   */
  protected function downloadRequestFormEntity(): DownloadRequestForm {
    if (!isset($this->downloadRequestForm)) {
      $this->downloadRequestForm = $this->getDownloadRequestFormFromRouteMatch();
    }

    return $this->downloadRequestForm;
  }

  /**
   * Get the download request form form route match.
   *
   * @return \Drupal\download_request_form\Entity\DownloadRequestForm
   *   The download request form configuration entity.
   *
   * @throws \Exception
   */
  protected function getDownloadRequestFormFromRouteMatch(): DownloadRequestForm {
    /** @var \Drupal\download_request_form\Entity\DownloadRequestForm $entity */
    $entity = $this->getRouteMatch()->getParameter('download_request_form');

    if (!$entity instanceof DownloadRequestFormInterface) {
      throw new \RuntimeException(
        'The download request form entity is required!'
      );
    }

    return $entity;
  }

  /**
   * Get the download item query instance
   *
   * @return \Drupal\Core\Entity\Query\QueryInterface
   *   The download item query instance.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Exception
   */
  protected function getDownloadItemQuery(): QueryInterface {
    return $this->getDownloadItemStorage()
      ->getQuery()
      ->condition('type', $this->getDownloadItemBundleType());
  }

  /**
   * Get download item storage instance.
   *
   * @return \Drupal\Core\Entity\EntityStorageInterface
   *   The entity storage instance.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getDownloadItemStorage(): EntityStorageInterface {
    return $this->entityTypeManager->getStorage('download_item');
  }
}
