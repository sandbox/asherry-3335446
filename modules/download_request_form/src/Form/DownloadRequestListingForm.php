<?php

declare(strict_types=1);

namespace Drupal\download_request_form\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\download_request\Entity\DownloadItem;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Field\FieldTypePluginManagerInterface;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class DownloadRequestListingForm
 */
class DownloadRequestListingForm extends DownloadRequestFormBase
{

    /**
     * @var \Drupal\Core\Entity\EntityFieldManagerInterface
     */
    protected $entityFieldManager;

    /**
     * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
     */
    protected $entityDisplayRepository;

    /**
     * @var \Drupal\Core\Field\FieldTypePluginManagerInterface
     */
    protected $fieldTypePluginManager;

    /**
     * Define the construct for the download request form multistep.
     *
     * @param \Drupal\Core\Render\RendererInterface                $renderer
     * @param \Drupal\Core\Entity\EntityTypeManagerInterface       $entity_type_manager
     * @param \Drupal\Core\Entity\EntityFieldManagerInterface      $entity_field_manager
     * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository
     * @param \Drupal\Core\Field\FieldTypePluginManagerInterface   $field_type_plugin_manager
     */
    public function __construct(
        RendererInterface $renderer,
        EntityTypeManagerInterface $entity_type_manager,
        EntityFieldManagerInterface $entity_field_manager,
        EntityDisplayRepositoryInterface $entity_display_repository,
        FieldTypePluginManagerInterface $field_type_plugin_manager
    ) {
        parent::__construct($renderer, $entity_type_manager);
        $this->entityFieldManager = $entity_field_manager;
        $this->entityDisplayRepository = $entity_display_repository;
        $this->fieldTypePluginManager = $field_type_plugin_manager;
    }

    /**
     * {@inheritDoc}
     */
    public static function create(ContainerInterface $container)
    {
        return new static(
            $container->get('renderer'),
            $container->get('entity_type.manager'),
            $container->get('entity_field.manager'),
            $container->get('entity_display.repository'),
            $container->get('plugin.manager.field.field_type')
        );
    }

    /**
     * {@inheritDoc}
     */
    public function getFormId()
    {
        return 'download_request_listing_form';
    }

    /**
     * {@inheritDoc}
     */
    public function buildForm(
        array $form,
        FormStateInterface $form_state
    ) {
        $form['#prefix'] = '<div id="download-request-form-results">';
        $form['#suffix'] = '</div>';

        $form += $this->form($form, $form_state);

        $form['actions']['#type'] = 'actions';
        $form['actions']['submit'] = [
        '#type' => 'submit',
        '#button_type' => 'primary',
        '#value' => $this->t('Continue'),
        ];

        return $form;
    }

    /**
     * Download request filter submit callback.
     *
     * @param array                                $form
     *   An array of form elements.
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *   The form state instance.
     */
    public function downloadRequestFilterSubmit(
        array $form,
        FormStateInterface $form_state
    ): void {
        $form_state->setRebuild();
    }

    /**
     * Download request filter ajax submit.
     *
     * @param array                                $form
     *   An array of form elements.
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *   The form state instance.
     *
     * @return array
     *   An array of form elements.
     */
    public function downloadRequestFilterAjaxSubmit(
        array $form,
        FormStateInterface $form_state
    ): array {
        return $form;
    }

    /**
     * {@inheritDoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state)
    {
        $items = array_filter($form_state->getValue('items', []));

        if (empty($items)) {
            $form_state->setError(
                $form['items'],
                $this->t('Please select at least one download item to request.')
            );
        }
    }

    /**
     * {@inheritDoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state): void
    {
        try {
            $options = [
            'query' => [
            'request_ids' => array_filter($form_state->getValue('items', [])),
            ],
            ];

            $form_state->setRedirect(
                'download_request_form.request.confirm_form',
                ['download_request_form' => $this->downloadRequestFormEntity()->id()],
                $options
            );
        } catch (\Exception $exception) {
            watchdog_exception('download_request_form', $exception);
        }
    }

    /**
     * Define the download item listing form elements.
     *
     * @param array                                $form
     *   An array of form elements.
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *   The form state instance.
     *
     * @return array
     *   An array of the form elements.
     *
     * @throws \Exception
     */
    protected function form(array $form, FormStateInterface $form_state)
    {
        $form += $this->buildDownloadItemFieldFilterForm($form_state);

        $headers = $this->getListingTableHeaders();
        $options = $this->getListingTableOptions($headers, $form_state);

        $form['items'] = [
        '#type' => 'tableselect',
        '#title_display' => 'invisible',
        '#header' => $headers,
        '#options' => $options,
        '#multiple' => true,
        '#responsive' => false,
        '#empty' => $this->t('There are no download items to request.')
        ];
        $form['pager']['#type'] = 'pager';

        return $form;
    }

    /**
     * Get download item listing table headers.
     *
     * @return array
     *   An array of table headers.
     *
     * @throws \Exception
     */
    protected function getListingTableHeaders(): array
    {
        return $this->buildEntityTableHeaders(
            $this->downloadRequestFormEntity()->showListingFields()
        );
    }

    /**
     * Get download item listing table options.
     *
     * @param array                                $headers
     *   An of table headers.
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *   The form state instance.
     *
     * @return array
     *   An array of table options.
     *
     * @throws \Exception
     */
    protected function getListingTableOptions(
        array &$headers,
        FormStateInterface $form_state
    ) {
        return $this->buildEntityTableRows(
            $this->loadDownloadItems($headers, $form_state),
            $this->downloadRequestFormEntity()->showListingFields()
        );
    }

    /**
     * Load the download item entities.
     *
     * @param array                                $headers
     *   An array of table headers.
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *   The form state instance.
     *
     * @return array|\Drupal\Core\Entity\EntityInterface[]
     *   An array of download items.
     *
     * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
     * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
     */
    protected function loadDownloadItems(
        array &$headers,
        FormStateInterface $form_state
    ): array {
        return $this->getDownloadItemStorage()->loadMultiple(
            $this->getDownloadItemIds($headers, $form_state)
        );
    }

    /**
     * Get the download item ids.
     *
     * @param array                                $headers
     *   An array of headers.
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *   The form state instance.
     *
     * @return array
     *   An array of download item ids.
     *
     * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
     * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
     * @throws \Exception
     */
    protected function getDownloadItemIds(
        array &$headers,
        FormStateInterface $form_state
    ): array {
        return $this->buildDownloadItemFilterQuery($form_state)
            ->pager($this->getDownloadItemDisplayLimit())
            ->tableSort($headers)
            ->execute();
    }

    /**
     * Build the download item field filter form.
     *
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *   The form state instance.
     *
     * @return array
     *   An array of the form elements.
     *
     * @throws \Exception
     */
    protected function buildDownloadItemFieldFilterForm(
        FormStateInterface $form_state
    ): array {
        $form = [];

        $form['filters'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Filters'),
        '#open' => true,
        '#tree' => true,
        ];
        $bundle = $this->getDownloadItemBundleType();
        $entity = DownloadItem::create(['type' => $bundle]);

        $request_form = $this->downloadRequestFormEntity();
        $form_display = $this->entityDisplayRepository->getFormDisplay(
            'download_item',
            $bundle,
            $request_form->itemFilterFormMode()
        );

        foreach (array_keys($request_form->showFilterFields()) as $field_name) {
            /**
     * @var \Drupal\Core\Field\WidgetInterface $widget 
*/
            if ($widget = $form_display->getRenderer($field_name)) {
                if (!$entity->hasField($field_name)) {
                    continue;
                }
                $items = $entity->get($field_name);
                $subform = ['#parents' => ['filters']];
                $widget_form = $widget->form($items, $subform, $form_state);
                $widget_form['widget']['#required'] = false;
                $form['filters'][$field_name] = $widget_form;
            }
        }

        $form['filters']['actions'] = [
        '#type' => 'actions',
        '#weight' => 100,
        ];
        $form['filters']['actions']['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Apply'),
        '#limit_validation_errors' => [],
        '#submit' => [
        [$this, 'downloadRequestFilterSubmit']
        ],
        '#ajax' => [
        'event' => 'click',
        'method' => 'replace',
        'wrapper' => 'download-request-form-results',
        'callback' => [$this, 'downloadRequestFilterAjaxSubmit']
        ]
        ];

        if (count($this->filterElementChildren($form['filters'], ['actions'])) === 0) {
            unset($form['filters']);
        }

        return $form;
    }

    /**
     * Build the download item query instance.
     *
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *   The form state instance.
     *
     * @return \Drupal\Core\Entity\Query\QueryInterface
     *   The entity query instance.
     *
     * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
     * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
     * @throws \Exception
     */
    protected function buildDownloadItemFilterQuery(
        FormStateInterface $form_state
    ): QueryInterface {
        $query = $this->getDownloadItemQuery();
        $request_form = $this->downloadRequestFormEntity();
        $entity = DownloadItem::create(['type' => $this->getDownloadItemBundleType()]);

        foreach (array_keys($request_form->showFilterFields()) as $field_name) {
            $input_values = $this->getFormStateInputValue(
                ['filters', $field_name],
                $form_state,
                []
            );
            $field_item = $this->fieldTypePluginManager
                ->createFieldItemList($entity, $field_name, $input_values);

            if ($field_values = $this->getFieldItemValues($field_item)) {
                  $query->condition($field_name, $field_values, 'IN');
            }
        }

        return $query;
    }

    /**
     * Get field item values.
     *
     * @param \Drupal\Core\Field\FieldItemListInterface $field_item
     *   The field item instance.
     *
     * @return array
     *   An array of the field values.
     */
    protected function getFieldItemValues(
        FieldItemListInterface $field_item
    ): array {
        $field_item->filterEmptyItems();

        if ($field_item->isEmpty()) {
            return [];
        }
        $field_values = explode(',', $field_item->getString());
        $field_values = !is_array($field_values) ? [$field_values] : $field_values;

        return array_filter(
            $field_values, function ($value) {
                return !in_array($value, ['_none']);
            }
        );
    }
}
