<?php

declare(strict_types=1);

namespace Drupal\download_request_form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\NestedArray;

/**
 * Define the form state input trait.
 */
trait FormStateInputTrait {

  /**
   * @param $key
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param null $default_value
   *
   * @return array|mixed|null
   */
  protected function getFormStateInputValue(
    $key,
    FormStateInterface $form_state,
    $default_value = NULL
  ) {
    $key = !is_array($key) ? [$key] : $key;

    $inputs = [
      $form_state->getValues(),
      $form_state->getUserInput(),
    ];

    foreach ($inputs as $input) {
      $key_exists = NULL;
      $value = NestedArray::getValue($input, $key, $key_exists);

      if ($key_exists) {
        return $value;
      }
    }

    return $default_value;
  }
}
