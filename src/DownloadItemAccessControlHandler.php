<?php

namespace Drupal\download_request;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityHandlerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\entity\EntityAccessControlHandler;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Access controller for the Download item entity.
 *
 * @see \Drupal\download_request\Entity\DownloadItem.
 */
class DownloadItemAccessControlHandler extends EntityAccessControlHandler implements EntityHandlerInterface {

  /**
   * @var EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var DownloadRequestManager
   */
  protected $downloadRequestManager;

  /**
   * @var EntityStorageInterface
   */
  protected $requestItemStorage;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeInterface $entity_type, EntityTypeManagerInterface $entity_type_manager, DownloadRequestManager $download_request_manager) {
    parent::__construct($entity_type);
    $this->entityTypeManager = $entity_type_manager;
    $this->downloadRequestManager = $download_request_manager;
    $this->requestItemStorage = $this->entityTypeManager->getStorage('download_request_item');
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static($entity_type,
      $container->get('entity_type.manager'),
      $container->get('download_request.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function checkFieldAccess($operation, FieldDefinitionInterface $field_definition, AccountInterface $account, FieldItemListInterface $items = NULL) {
    $result = parent::checkFieldAccess($operation, $field_definition, $account, $items);
    if (!$items || !$items->getEntity()) {
      return $result;
    }
    $field_name = $field_definition->getName();
    if (!$entity = $items->getEntity()) {
      return $result;
    }
    // Access will be allowed by default because the parent entity should be allowed.
    if ($this->downloadRequestManager->getDownloadItemAccessField($entity) == $field_name) {
      return AccessResult::allowedIf($this->downloadRequestManager->userHasDownload($entity, $account))
        ->orIf($this->hasAdminPermission($entity, $account))
        ->cachePerUser();
    }
    return $result;
  }

  /**
   * Helper function to determine if an admin can bypass access fields.
   *
   * @param EntityInterface $entity
   *   The parent entity of the access field.
   * @param AccountInterface $account
   *   Current logged in user.
   *
   * @return AccessResult|\Drupal\Core\Access\AccessResultReasonInterface
   */
  protected function hasAdminPermission(EntityInterface $entity, AccountInterface $account) {
    $admin_permission = $entity->getEntityType()->getAdminPermission();
    return AccessResult::allowedIfHasPermission($account, $admin_permission);
  }
}
