<?php

namespace Drupal\download_request;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Download item entities.
 *
 * @ingroup download_request
 */
class DownloadItemListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    return [
      'name' => $this->t('Name'),
      'author' => $this->t('Author'),
      'updated' => $this->t('Updated'),
    ] + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\download_request\Entity\DownloadItem $entity */
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.download_item.edit_form',
      ['download_item' => $entity->id()]
    );
    $row['author'] = $entity->getOwner()->label();
    $row['updated'] = $this->timestampToFormattedDate($entity->getChangedTime());

    return $row + parent::buildRow($entity);
  }

  /**
   * Convert the timestamp to a formatted date.
   *
   * @param $timestamp
   *   A valid timestamp.
   *
   * @return string
   *   The formatted timestamp.
   */
  protected function timestampToFormattedDate($timestamp) {
    return DrupalDateTime::createFromTimestamp($timestamp)
      ->format('m/j/y - H:i:s');
  }
}
