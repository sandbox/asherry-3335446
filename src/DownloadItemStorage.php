<?php

namespace Drupal\download_request;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\download_request\Entity\DownloadItemInterface;

/**
 * Defines the storage handler class for Download item entities.
 *
 * This extends the base storage class, adding required special handling for
 * Download item entities.
 *
 * @ingroup download_request
 */
class DownloadItemStorage extends SqlContentEntityStorage implements DownloadItemStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(DownloadItemInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {download_item_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {download_item_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(DownloadItemInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {download_item_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('download_item_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
