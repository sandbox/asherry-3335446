<?php

namespace Drupal\download_request;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\download_request\Entity\DownloadItemInterface;

/**
 * Defines the storage handler class for Download item entities.
 *
 * This extends the base storage class, adding required special handling for
 * Download item entities.
 *
 * @ingroup download_request
 */
interface DownloadItemStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Download item revision IDs for a specific Download item.
   *
   * @param \Drupal\download_request\Entity\DownloadItemInterface $entity
   *   The Download item entity.
   *
   * @return int[]
   *   Download item revision IDs (in ascending order).
   */
  public function revisionIds(DownloadItemInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Download item author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Download item revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\download_request\Entity\DownloadItemInterface $entity
   *   The Download item entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(DownloadItemInterface $entity);

  /**
   * Unsets the language for all Download item with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
