<?php

namespace Drupal\download_request;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Access controller for the Download request item entity.
 *
 * @see \Drupal\download_request\Entity\DownloadRequestItem.
 */
class DownloadRequestItemAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    $result = parent::checkAccess($entity, $operation, $account);

    if ($result->isNeutral() && ($download_request = $entity->getParentEntity())) {
      $result = $download_request->access($operation, $account, TRUE);
      $debug = 'true';
    }

    return $result;
  }


}
