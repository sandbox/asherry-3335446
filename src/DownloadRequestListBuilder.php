<?php

namespace Drupal\download_request;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityListBuilder;

/**
 * Defines a class to build a listing of Download request entities.
 */
class DownloadRequestListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    return [
      'id' => $this->t('ID'),
      'author' => $this->t('Author'),
      'updated' => $this->t('Updated'),
    ] + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\download_request\Entity\DownloadRequest $entity */
    $row['id'] = $entity->id();
    $owner = $entity->getOwner();
    $row['author'] = $entity->getOwner() ? $entity->getOwner()->label() : '';
    $row['updated'] = $this->timestampToFormattedDate($entity->getChangedTime());
    return $row + parent::buildRow($entity);
  }

  /**
   * Convert the timestamp to a formatted date.
   *
   * @param $timestamp
   *   A valid timestamp.
   *
   * @return string
   *   The formatted timestamp.
   */
  protected function timestampToFormattedDate($timestamp) {
    return DrupalDateTime::createFromTimestamp($timestamp)
      ->format('m/j/y - H:i:s');
  }

}
