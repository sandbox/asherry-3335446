<?php

namespace Drupal\download_request;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\download_request\Entity\DownloadItemInterface;
use Drupal\user\Entity\User;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class DownloadRequestManager implements ContainerInjectionInterface {

  use DependencySerializationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The download request storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $downloadRequestStorage;

  /**
   * The user storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $userStorage;

  /**
   * Constructs a \Drupal\download_request\DownloadRequestManager object.
   *
   * @param EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->downloadRequestStorage = $this->entityTypeManager->getStorage('download_request');
    $this->userStorage = $this->entityTypeManager->getStorage('user');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('entity_type.manager'));
  }

  /**
   * Get Download Item entities for a given user.
   *
   * @param AccountInterface $account
   *   The user account.
   * @return mixed
   */
  public function getUserDownloads(AccountInterface $account) {
    $user = $this->ensureUser($account);
    return $user->get('download_request_downloads')->referencedEntities();
  }

  /**
   * Converts an AccountProxy into a fully loaded user if needed.
   *
   * @param AccountInterface $account
   *   The user account.
   *
   * @return \Drupal\Core\Entity\EntityBase|\Drupal\Core\Entity\EntityInterface|AccountInterface|UserInterface|null
   */
  private function ensureUser(AccountInterface $account) {
    if ($account instanceof UserInterface) {
      return $account;
    }
    return User::load($account->id());
  }

  /**
   * @param AccountInterface $account
   *   The user account.
   *
   * @return array
   */
  public function refreshUserDownloads(AccountInterface $account) {
    $user = $this->ensureUser($account);
    $items = [];

    $request_ids = $this->downloadRequestStorage->getQuery()
      ->accessCheck(FALSE)
      ->condition('user', $user->id())
      ->execute();

    $requests = $this->downloadRequestStorage->loadMultiple(array_values($request_ids));
    foreach ($requests as $request) {
      $entity_type_id = $request->getEntityType()->getBundleEntityType();
      $request_type = $this->entityTypeManager->getStorage($entity_type_id)->load($request->bundle());
      $state = $request_type->getAccessFieldWorkflowState();

      foreach ($request->request_items as $item) {
        $request_item = $item->entity;
        if ($request_item->get('state')->first()->getId() != $state) {
          continue;
        }
        $items[] = $request_item->download_item->entity;
      }
    }

    $user->set('download_request_downloads', $items);
    $user->save();

    return $items;
  }

  /**
   * Check if a user has access to a specific Download Item entity.
   *
   * @param DownloadItemInterface $download_item
   *   The Download Item.
   * @param AccountInterface $account
   *   The user account.
   *
   * @return bool
   */
  public function userHasDownload(DownloadItemInterface $download_item, AccountInterface $account) {
    $user = $this->ensureUser($account);
    return array_reduce($user->download_request_downloads->referencedEntities(), function($carry, $item) use ($download_item) {
      if ($item->id() == $download_item->id()) {
        $carry = TRUE;
      }
      return $carry;
    }, FALSE);
  }

  /**
   * Gets an array of users that have access to this DownloadItem.
   *
   * @param DownloadItemInterface $download_item
   *   The Download Item.
   *
   * @return EntityInterface[]
   */
  public function getDownloadItemUsers(DownloadItemInterface $download_item) {
    $result = $this->userStorage->getQuery()
      ->condition('status', 1)
      ->accessCheck(FALSE)
      ->condition('download_request_downloads', $download_item->id(), 'IN')
      ->execute();

    return !empty($result) ? User::loadMultiple(array_values($result)) : [];
  }

  /**
   * Get the access field for the Download Item entity.
   *
   * @param DownloadItemInterface $download_item
   *   The Download Item.
   *
   * @return string|null
   */
  public function getDownloadItemAccessField(DownloadItemInterface $download_item) {
    $entity_type_id = $download_item->getEntityType()->getBundleEntityType();
    if (!$bundle = $download_item->bundle()) {
      return NULL;
    }
    if (!$download_item_type = $this->entityTypeManager->getStorage($entity_type_id)->load($bundle)) {
      return NULL;
    }
    return $download_item_type->getAccessField();
  }
}
