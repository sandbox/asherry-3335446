<?php

namespace Drupal\download_request;

use Drupal\Console\Command\Shared\TranslationTrait;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\state_machine\WorkflowManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class DownloadRequestPermissions implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var WorkflowManagerInterface
   */
  protected $workflowManager;

  /**
   * Constructs a new DownloadRequestPermissions object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param WorkflowManagerInterface $workflow_manager
   *   The workflow manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, WorkflowManagerInterface $workflow_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->workflowManager = $workflow_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.workflow')
    );
  }

  /**
   * Builds a list of permissions for the participating entity types.
   *
   * @return array
   *   The permissions.
   */
  public function buildPermissions() {
    $download_item_types = $this->entityTypeManager->getStorage('download_item_type')->loadMultiple();
    $download_request_types = $this->entityTypeManager->getStorage('download_request_type')->loadMultiple();

    return array_reduce($download_item_types, function($carry, $download_item_type) use ($download_request_types) {
      foreach ($download_request_types as $bundle => $download_request_type) {
        $field_id = $download_item_type->getAccessField();
        $state_id = $download_request_type->getAccessFieldWorkflowState();
        $carry["view $bundle $field_id field in state $state_id"] = [
          'title' => $this->t("View @bundle_id @field_id in state @state_id", [
            '@bundle_id' => $bundle,
            '@field_id' => $field_id,
            '@state_id' => $state_id,
          ]),
          'provider' => 'download_request',
        ];
      }
      return $carry;
    }, []);
  }

}
