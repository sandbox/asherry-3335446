<?php

namespace Drupal\download_request;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for download_request.
 */
class DownloadRequestTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
