<?php

namespace Drupal\download_request\Entity;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\entity\Revision\RevisionableContentEntityBase;
use Drupal\user\EntityOwnerTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Defines the Download item entity.
 *
 * @ContentEntityType(
 *   id = "download_item",
 *   label = @Translation("Download item"),
 *   label_collection = @Translation("Download items"),
 *   label_plural = @Translation("download items"),
 *   bundle_label = @Translation("Download item type"),
 *   bundle_entity_type = "download_item_type",
 *   base_table = "download_item",
 *   data_table = "download_item_field_data",
 *   revision_table = "download_item_revision",
 *   revision_data_table = "download_item_field_revision",
 *   translatable = TRUE,
 *   permission_granularity = "bundle",
 *   admin_permission = "administer download item entities",
 *   field_ui_base_route = "entity.download_item_type.edit_form",
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_user",
 *     "revision_created" = "revision_created",
 *     "revision_log_message" = "revision_log"
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "bundle" = "type",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "owner" = "user",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\download_request\DownloadItemListBuilder",
 *     "views_data" = "Drupal\entity\EntityViewsData",
 *     "translation" = "Drupal\download_request\DownloadItemTranslationHandler",
 *     "form" = {
 *       "default"  = "Drupal\download_request\Form\DownloadItem\DownloadItemForm",
 *       "add"      = "Drupal\download_request\Form\DownloadItem\DownloadItemForm",
 *       "edit"     = "Drupal\download_request\Form\DownloadItem\DownloadItemForm",
 *       "delete"   = "Drupal\download_request\Form\DownloadItem\DownloadItemDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\download_request\Routing\AdminHtmlRouteProvider",
 *       "revision" = "Drupal\download_request\Routing\RevisionRouteProvider",
 *       "delete-multiple" = "Drupal\entity\Routing\DeleteMultipleRouteProvider",
 *     },
 *     "local_action_provider" = {
 *       "collection" = "\Drupal\entity\Menu\EntityCollectionLocalActionProvider",
 *     },
 *     "local_task_provider" = {
 *       "default" = "\Drupal\entity\Menu\DefaultEntityLocalTaskProvider",
 *     },
 *     "access" = "\Drupal\download_request\DownloadItemAccessControlHandler",
 *     "permission_provider" = "\Drupal\download_request\EntityPermissionProvider",
 *   },
 *   links = {
 *     "canonical"            = "/download-item/{download_item}",
 *     "collection"           = "/admin/content/download-request/items",
 *     "add-page"             = "/admin/content/download-items/add",
 *     "add-form"             = "/admin/content/download-items/add/{download_item_type}",
 *     "edit-form"            = "/download-item/{download_item}/edit",
 *     "delete-form"          = "/download-item/{download_item}/delete",
 *     "version-history"      = "/download-item/{download_item}/revisions",
 *     "revision"             = "/download-item/{download_item}/revisions/{download_item_revision}/view",
 *     "revision-revert-form" = "/download-item/{download_item}/revisions/{download_item_revision}/revert",
 *     "revision-delete-form" = "/download-item/{download_item}/revisions/{download_item_revision}/delete"
 *   }
 * )
 */
class DownloadItem extends RevisionableContentEntityBase implements DownloadItemInterface {

  use EntityChangedTrait;
  use EntityOwnerTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    foreach (array_keys($this->getTranslationLanguages()) as $langcode) {
      $translation = $this->getTranslation($langcode);

      // If no owner has been set explicitly, make the anonymous user the owner.
      if (!$translation->getOwner()) {
        $translation->setOwnerId(0);
      }
    }

    // If no revision author has been set explicitly,
    // make the download_item owner the revision author.
    if (!$this->getRevisionUser()) {
      $this->setRevisionUserId($this->getOwnerId());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);
    $fields += static::ownerBaseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Name'))
      ->setDescription(new TranslatableMarkup('The name of the download item.'))
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setSettings([
        'max_length' => 128,
        'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    if (isset($fields['status'])) {
      $fields['status']->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ]);
    }

    $fields['path'] = BaseFieldDefinition::create('path')
      ->setLabel(new TranslatableMarkup('URL alias'))
      ->setTranslatable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'path',
        'weight' => 30,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setComputed(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(new TranslatableMarkup('Created'))
      ->setDescription(new TranslatableMarkup('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(new TranslatableMarkup('Changed'))
      ->setDescription(new TranslatableMarkup('The time that the entity was last edited.'));

    $fields['revision_translation_affected'] = BaseFieldDefinition::create('boolean')
      ->setLabel(new TranslatableMarkup('Revision translation affected'))
      ->setDescription(new TranslatableMarkup('Indicates if the last edit of a translation belongs to current revision.'))
      ->setReadOnly(TRUE)
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE);

    return $fields;
  }

}
