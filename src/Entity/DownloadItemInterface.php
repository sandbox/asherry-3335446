<?php

namespace Drupal\download_request\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Download item entities.
 *
 * @ingroup download_request
 */
interface DownloadItemInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Download item name.
   *
   * @return string
   *   Name of the Download item.
   */
  public function getName();

  /**
   * Sets the Download item name.
   *
   * @param string $name
   *   The Download item name.
   *
   * @return \Drupal\download_request\Entity\DownloadItemInterface
   *   The called Download item entity.
   */
  public function setName($name);

  /**
   * Gets the Download item creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Download item.
   */
  public function getCreatedTime();

  /**
   * Sets the Download item creation timestamp.
   *
   * @param int $timestamp
   *   The Download item creation timestamp.
   *
   * @return \Drupal\download_request\Entity\DownloadItemInterface
   *   The called Download item entity.
   */
  public function setCreatedTime($timestamp);
}
