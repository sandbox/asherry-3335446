<?php

namespace Drupal\download_request\Entity;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Download item type entity.
 *
 * @ConfigEntityType(
 *   id = "download_item_type",
 *   label = @Translation("Download item type"),
 *   label_collection = @Translation("Download item type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\download_request\DownloadItemTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\download_request\Form\DownloadItem\DownloadItemTypeForm",
 *       "edit" = "Drupal\download_request\Form\DownloadItem\DownloadItemTypeForm",
 *       "delete" = "Drupal\download_request\Form\DownloadItem\DownloadItemTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\download_request\DownloadItemTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "download_item_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "download_item",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "access_field" = "access_field"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "access_field"
 *   },
 *   links = {
 *     "collection" = "/admin/structure/download_item_type",
 *     "add-form" = "/admin/structure/download_item_type/add",
 *     "edit-form" = "/admin/structure/download_item_type/{download_item_type}",
 *     "delete-form" = "/admin/structure/download_item_type/{download_item_type}/delete",
 *   }
 * )
 */
class DownloadItemType extends ConfigEntityBundleBase implements DownloadItemTypeInterface {

  /**
   * The Download item type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Download item type label.
   *
   * @var string
   */
  protected $label;

  protected $access_field;

  public function setAccessField($access_field) {
    $this->access_field = $access_field;
  }

  public function getAccessField() {
    return $this->access_field;
  }
}
