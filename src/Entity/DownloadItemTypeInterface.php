<?php

namespace Drupal\download_request\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Download item type entities.
 */
interface DownloadItemTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
