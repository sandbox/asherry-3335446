<?php

namespace Drupal\download_request\Entity;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EditorialContentEntityBase;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\user\EntityOwnerTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\download_request_form\Contract\DownloadRequestFactoryInterface;

/**
 * Defines the Download request entity.
 *
 * @ContentEntityType(
 *   id = "download_request",
 *   label = @Translation("Download request"),
 *   label_plural = @Translation("download requests"),
 *   label_collection = @Translation("Download request"),
 *   bundle_label = @Translation("Download request type"),
 *   bundle_entity_type = "download_request_type",
 *   field_ui_base_route = "entity.download_request_type.edit_form",
 *   base_table = "download_request",
 *   data_table = "download_request_field_data",
 *   revision_table = "download_request_revision",
 *   revision_data_table = "download_request_field_revision",
 *   translatable = TRUE,
 *   show_revision_ui = FALSE,
 *   permission_granularity = "bundle",
 *   admin_permission = "administer download requests",
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_user",
 *     "revision_created" = "revision_created",
 *     "revision_log_message" = "revision_log"
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "bundle" = "type",
 *     "uuid" = "uuid",
 *     "owner" = "user",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\download_request\DownloadRequestListBuilder",
 *     "views_data" = "Drupal\download_request\EntityViewsData",
 *     "translation" = "Drupal\download_request\DownloadRequestTranslationHandler",
 *     "form" = {
 *       "default" = "Drupal\download_request\Form\DownloadRequest\DownloadRequestForm",
 *       "add" = "Drupal\download_request\Form\DownloadRequest\DownloadRequestForm",
 *       "edit" = "Drupal\download_request\Form\DownloadRequest\DownloadRequestForm",
 *       "delete" = "Drupal\download_request\Form\DownloadRequest\DownloadRequestDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\download_request\Routing\AdminHtmlRouteProvider",
 *       "revision" = "\Drupal\download_request\Routing\RevisionRouteProvider",
 *     },
 *     "local_action_provider" = {
 *       "collection" = "\Drupal\entity\Menu\EntityCollectionLocalActionProvider",
 *     },
 *     "local_task_provider" = {
 *       "default" = "\Drupal\entity\Menu\DefaultEntityLocalTaskProvider",
 *     },
 *     "access" = "Drupal\entity\UncacheableEntityAccessControlHandler",
 *     "permission_provider" = "Drupal\entity\UncacheableEntityPermissionProvider",
 *   },
 *   links = {
 *     "canonical"             = "/download-request/{download_request}",
 *     "collection"            = "/admin/content/download-request/requests",
 *     "add-page"              = "/admin/content/download-requests/add",
 *     "add-form"              = "/admin/content/download-requests/add/{download_request_type}",
 *     "edit-form"             = "/download-request/{download_request}/edit",
 *     "delete-form"           = "/download-request/{download_request}/delete",
 *     "version-history"       = "/download-request/{download_request}/revisions",
 *     "revision"              = "/download-request/{download_request}/revisions/{download_request_revision}/view",
 *     "revision-revert-form"  = "/download-request/{download_request}/revisions/{download_request_revision}/revert",
 *     "revision-delete-form"  = "/download-request/{download_request}/revisions/{download_request_revision}/delete"
 *   }
 * )
 */
class DownloadRequest extends EditorialContentEntityBase implements DownloadRequestInterface {

  use EntityChangedTrait;
  use EntityOwnerTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * Create with download request items.
   *
   * @param string $bundle
   *   The download request bundle.
   * @param array $values
   *   An array of download request values.
   * @param array $items
   *   An array of download item identifiers.
   *
   * @return \Drupal\download_request\Entity\DownloadRequest
   */
  public static function createDownloadRequestWithItems(
    string $bundle,
    array $values,
    array $items
  ): DownloadRequest {
    $values += [
      'type' => $bundle,
      'request_items' => DownloadRequestItem::createDownloadRequestItems($items, $bundle)
    ];

    $download_request = static::create($values);
    foreach ($download_request->get('request_items') as $request_item) {
      $request_item->entity->tempParentEntity = $download_request;
    }
    return $download_request;
  }

  /**
   * Group field items by workflow state ID.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface
   *   The field item list object.
   *
   * @return \Drupal\Core\Field\FieldItemInterface[]
   *   Field items keyed by workflow state ID.
   */
  public static function getGroupedRequestItems(FieldItemListInterface $items, $limit = FALSE) {
    $items = clone $items;
    if ($limit && $limit_items = $items->getEntity()->limitItems) {
      $items->filter(function($item) use ($limit_items) {
        return $item->target_id ? in_array($item->target_id, $limit_items) : TRUE;
      });
    }


    $items_by_state_id = [];
    foreach ($items as $item) {
      if (!$entity = $item->entity) {
        continue;
      }
      if (!$entity->state || !$entity->state->first()) {
        continue;
      }
      $items_by_state_id[$entity->state->first()->getId()][] = $entity->download_item->entity;
    }
    return $items_by_state_id;
  }

  /**
   * Group field items by workflow state ID.
   *
   * @return \Drupal\Core\Field\FieldItemInterface[]
   *   Field items keyed by workflow state ID.
   */
  public function getGroupedItems($limit = FALSE) {
    return static::getGroupedRequestItems($this->get('request_items'), $limit);
  }

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel) {
    $uri_route_parameters = parent::urlRouteParameters($rel);

    if ($rel === 'revision_revert' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }
    elseif ($rel === 'revision_delete' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }

    return $uri_route_parameters;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    foreach (array_keys($this->getTranslationLanguages()) as $langcode) {
      $translation = $this->getTranslation($langcode);

      // If no owner has been set explicitly, make the anonymous user the owner.
      if (!$translation->getOwner()) {
        $translation->setOwnerId(0);
      }
    }

    // If no revision author has been set explicitly,
    // make the download_request owner the revision author.
    if (!$this->getRevisionUser()) {
      $this->setRevisionUserId($this->getOwnerId());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    parent::postSave($storage, $update);
    foreach ($this->request_items as $item) {
      if ($item->entity->parent_entity_id->value) {
        continue;
      }
      $item->entity->set('parent_entity_id', $this->id());
      $item->entity->save();
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function preDelete(EntityStorageInterface $storage, array $entities) {
    $request_items = [];
    foreach ($entities as $entity) {
      foreach ($entity->request_items as $download_item) {
        if (!$download_item->entity) {
          continue;
        }
        $request_items[] = $download_item->entity;
      }
    }
    if (!empty($request_items)) {
      \Drupal::service('entity_type.manager')->getStorage('download_request_item')->delete($request_items);
    }
    parent::preDelete($storage, $entities);
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);
    $fields += static::ownerBaseFieldDefinitions($entity_type);

    $fields[$entity_type->getKey('owner')]->setDisplayConfigurable('view', TRUE)
      ->setLabel(new TranslatableMarkup('Submitted by'))
      ->setDisplayConfigurable('form', TRUE);

    $fields['request_items'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(new TranslatableMarkup('Request items'))
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setRevisionable(TRUE)
      ->setSettings([
        'handler' => 'default:download_request_item',
        'target_type' => 'download_request_item'
      ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'entity_reference_entity_view',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'placeholder' => '',
        ]
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    if (isset($fields['status'])) {
      $fields['status']->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ]);
    }

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(new TranslatableMarkup('Submitted on'))
      ->setDescription(new TranslatableMarkup(
        'The time that the entity was created.'
      ))
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(new TranslatableMarkup('Changed'))
      ->setDescription(new TranslatableMarkup(
        'The time that the entity was last edited.'
      ));

    $fields['revision_translation_affected'] = BaseFieldDefinition::create('boolean')
      ->setLabel(new TranslatableMarkup('Revision translation affected'))
      ->setDescription(new TranslatableMarkup(
        'Indicates if the last edit of a translation belongs to current revision.'
      ))
      ->setReadOnly(TRUE)
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE);

    return $fields;
  }

}
