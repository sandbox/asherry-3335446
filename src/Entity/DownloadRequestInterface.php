<?php

namespace Drupal\download_request\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Download request entities.
 *
 * @ingroup download_request
 */
interface DownloadRequestInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Download request name.
   *
   * @return string
   *   Name of the Download request.
   */
  public function getName();

  /**
   * Sets the Download request name.
   *
   * @param string $name
   *   The Download request name.
   *
   * @return \Drupal\download_request\Entity\DownloadRequestInterface
   *   The called Download request entity.
   */
  public function setName($name);

  /**
   * Gets the Download request creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Download request.
   */
  public function getCreatedTime();

  /**
   * Sets the Download request creation timestamp.
   *
   * @param int $timestamp
   *   The Download request creation timestamp.
   *
   * @return \Drupal\download_request\Entity\DownloadRequestInterface
   *   The called Download request entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the Download request revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Download request revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\download_request\Entity\DownloadRequestInterface
   *   The called Download request entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Download request revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Download request revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\download_request\Entity\DownloadRequestInterface
   *   The called Download request entity.
   */
  public function setRevisionUserId($uid);

}
