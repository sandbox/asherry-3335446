<?php

namespace Drupal\download_request\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\RevisionLogEntityTrait;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Tests\Core\Field\TestBaseFieldDefinitionInterface;

/**
 * Defines the Download request item entity.
 *
 * @ContentEntityType(
 *   id = "download_request_item",
 *   label = @Translation("Download request item"),
 *   base_table = "download_request_item",
 *   revision_table = "download_request_item_revision",
 *   revision_data_table = "download_request_item_field_revision",
 *   field_ui_base_route = "download_request_item.settings",
 *   admin_permission = "administer download requests",
 *   translatable = FALSE,
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_user",
 *     "revision_created" = "revision_created",
 *     "revision_log_message" = "revision_log"
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "uuid" = "uuid",
 *     "langcode" = "langcode",
 *   },
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\download_request\DownloadRequestItemListBuilder",
 *     "views_data" = "Drupal\download_request\EntityViewsData",
 *     "form" = {
 *       "settings" = "Drupal\download_request\Form\DownloadRequest\DownloadRequestItemSettingsForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\download_request\Routing\AdminHtmlRouteProvider",
 *       "revision" = "\Drupal\entity\Routing\RevisionRouteProvider",
 *     },
 *     "local_action_provider" = {
 *       "collection" = "\Drupal\entity\Menu\EntityCollectionLocalActionProvider",
 *     },
 *     "local_task_provider" = {
 *       "default" = "\Drupal\entity\Menu\DefaultEntityLocalTaskProvider",
 *     },
 *     "access" = "\Drupal\download_request\DownloadRequestItemAccessControlHandler",
 *   },
 *   links = {
 *     "settings-form" = "/admin/structure/download-request/download-request-item",
 *   }
 * )
 */
class DownloadRequestItem extends ContentEntityBase implements DownloadRequestItemInterface {

  use EntityChangedTrait;
  use RevisionLogEntityTrait;
  use StringTranslationTrait;

  /**
   * @var string
   */
  public const REQUEST_ITEM_DEFAULT_STATE = 'submitted';

  /**
   * Create download request items.
   *
   * @param array $item_ids
   *   An array of download item identifiers.
   *
   * @return array
   *   An array of fully instantiated download request item instances.
   */
  public static function createDownloadRequestItems(array $item_ids, $parent_bundle = NULL): array {
    $items = [];

    foreach ($item_ids as $item_id) {
      $entity = static::create([
        'state' => 'new',
        'parent_entity_bundle' => $parent_bundle,
        'download_item' => [
          'target_id' => $item_id,
        ],
      ]);

      $entity->set('state', static::REQUEST_ITEM_DEFAULT_STATE);

      $items[] = [
        'entity' => $entity,
      ];
    }

    return $items;
  }

  /**
   * {@inheritDoc}
   */
  public function label() {
    return $this->t('@download_item: @state', [
      '@state' => $this->get('state')->value,
      '@download_item' => $this->get('download_item')->entity ? $this->get('download_item')->entity->label() : $this->t('missing download item'),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel) {
    $uri_route_parameters = parent::urlRouteParameters($rel);

    if ($rel === 'revision_revert' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }
    elseif ($rel === 'revision_delete' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }

    return $uri_route_parameters;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  public function getParentEntityBundle() {
    if ($entity = $this->get('parent_entity_bundle')->entity) {
      return $entity;
    }
    if ($target_id = $this->get('parent_entity_bundle')->target_id) {
      return $this->entityTypeManager()->getStorage('download_request_type')->load($target_id);
    }
    return NULL;
  }

  /**
   * @see DownloadRequestItem::createDownloadRequestWithItems()
   *
   * @return \Drupal\Core\Entity\EntityInterface|\Drupal\Core\Field\FieldItemListInterface|false|mixed|null
   */
  public function getParentEntity() {
    // DownloadRequestItem is saved initially before parent DownloadRequest.
    // This is set during create and will be isNew().
    if ($this->tempParentEntity) {
      return $this->tempParentEntity;
    }
    if ($target_id = $this->parent_entity_id->value) {
      return $this->entityTypeManager()->getStorage('download_request')->load($target_id);
    }

    $requests = $this->entityTypeManager()->getStorage('download_request')->loadByProperties([
      'request_items.target_id' => $this->id(),
    ]);

    return !empty($requests) ? reset($requests) : NULL;
  }

  /**
   * Get the workflow plugin id for the parent download_request bundle.
   *
   * @param DownloadRequestItemInterface $entity
   *
   * @return string
   */
  public static function getWorkflowId(DownloadRequestItemInterface $entity) {
    if (!$bundle = $entity->getParentEntityBundle()) {
      $bundle = self::getParentEntityBundleFromRoute();
    }
    return $bundle ? $bundle->getWorkflowId() : 'download_request_default';
  }

  public static function getWorkflowDefaultValue(DownloadRequestItemInterface $download_request_item, FieldDefinitionInterface $base_field_definition) {
    $bundle = $download_request_item->getParentEntityBundle() ?? self::getParentEntityBundleFromRoute();
    return $bundle ? $bundle->getWorkflowStateDefaultValue() : 'new';
  }

  protected static function getParentEntityBundleFromRoute() {
    if ($bundle = \Drupal::routeMatch()->getParameter('download_request_type')) {
      return $bundle;
    }
    if ($parent_entity = self::getParentEntityFromRoute()) {
      $bundle_entity_type_id = $parent_entity->getEntityType()->getBundleEntityType();
      return \Drupal::service('entity_type.manager')->getStorage($bundle_entity_type_id)->load($parent_entity->bundle());
    }
    return NULL;
  }

  /**
   * @return \Drupal\download_request\Entity\DownloadRequest|null
   */
  protected static function getParentEntityFromRoute() {
    return \Drupal::routeMatch()->getParameter('download_request');
  }

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage, array &$values) {
    // If the parent entity type is set in the url (on the add form), use that.
    if (empty($values['parent_entity_bundle']) && ($type = self::getParentEntityBundleFromRoute())) {
      $values['parent_entity_bundle'] = $type;
    }
    if (empty($values['parent_entity_bundle']) && ($parent_entity = self::getParentEntityFromRoute())) {
      $values['parent_entity_id'] = $parent_entity->id();
      $values['parent_entity_bundle'] = $parent_entity->bundle();
    }
    parent::preCreate($storage, $values);
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the revision metadata fields.
    $fields += static::revisionLogBaseFieldDefinitions($entity_type);

    $fields['download_item'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(new TranslatableMarkup('Download item'))
      ->setRequired(TRUE)
      ->setCardinality(1)
      ->setRevisionable(TRUE)
      ->setSettings([
        'handler' => 'default:download_item',
        'target_type' => 'download_item'
      ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'entity_reference_entity_view',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'placeholder' => '',
        ]
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['parent_entity_bundle'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(new TranslatableMarkup('Parent Entity Bundle'))
      ->setCardinality(1)
      ->setSettings([
        'handler' => 'default:download_request_type',
        'target_type' => 'download_request_type'
      ]);

    $fields['parent_entity_id'] = BaseFieldDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Parent Entity ID'))
      ->setCardinality(1);

    $fields['state'] = BaseFieldDefinition::create('state')
      ->setLabel(new TranslatableMarkup('State'))
      ->setRequired(TRUE)
      ->setDescription(new TranslatableMarkup(
        'The current state of the requested item.'
      ))
      ->setDefaultValueCallback(static::class . '::getWorkflowDefaultValue')
      ->setSetting('workflow_callback', ['\Drupal\download_request\Entity\DownloadRequestItem', 'getWorkflowId'])
      ->setRevisionable(TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(new TranslatableMarkup('Created'))
      ->setDescription(new TranslatableMarkup(
        'The time that the entity was created.'
      ));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(new TranslatableMarkup('Changed'))
      ->setDescription(new TranslatableMarkup(
        'The time that the entity was last edited.'
      ));

    return $fields;
  }

}
