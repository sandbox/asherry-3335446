<?php

namespace Drupal\download_request\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface for defining Download request item entities.
 *
 * @ingroup download_request
 */
interface DownloadRequestItemInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Download request item creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Download request item.
   */
  public function getCreatedTime();

  /**
   * Sets the Download request item creation timestamp.
   *
   * @param int $timestamp
   *   The Download request item creation timestamp.
   *
   * @return \Drupal\download_request\Entity\DownloadRequestItemInterface
   *   The called Download request item entity.
   */
  public function setCreatedTime($timestamp);
}
