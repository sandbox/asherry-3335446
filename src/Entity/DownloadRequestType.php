<?php

namespace Drupal\download_request\Entity;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Download request type entity.
 *
 * @ConfigEntityType(
 *   id = "download_request_type",
 *   label = @Translation("Download request type"),
 *   label_collection = @Translation("Download request type"),
 *   config_prefix = "download_request_type",
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid",
 *     "workflow_id",
 *     "access_field_workflow_state",
 *     "workflow_state_default_value"
 *   },
 *   admin_permission = "administer site configuration",
 *   bundle_of = "download_request",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "workflow_id" = "workflow_id"
 *   },
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\download_request\DownloadRequestTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\download_request\Form\DownloadRequest\DownloadRequestTypeForm",
 *       "edit" = "Drupal\download_request\Form\DownloadRequest\DownloadRequestTypeForm",
 *       "delete" = "Drupal\download_request\Form\DownloadRequest\DownloadRequestTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\download_request\DownloadRequestTypeHtmlRouteProvider",
 *     },
 *   },
 *   links = {
 *     "collection" = "/admin/structure/download_request_type",
 *     "add-form" = "/admin/structure/download_request_type/add",
 *     "edit-form" = "/admin/structure/download_request_type/{download_request_type}",
 *     "delete-form" = "/admin/structure/download_request_type/{download_request_type}/delete"
 *   }
 * )
 */
class DownloadRequestType extends ConfigEntityBundleBase implements DownloadRequestTypeInterface {

  const WORKFLOW_GROUP_ID = 'download_request';

  /**
   * The Download request type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Download request type label.
   *
   * @var string
   */
  protected $label;

  /**
   * Workflow type id.
   *
   * @var string
   */
  protected $workflow_id;

  /**
   * Workflow state default value.
   *
   * @var string
   */
  protected $workflow_state_default_value;

  protected $access_field_workflow_state;

  public function getWorkflowId() {
    return $this->workflow_id;
  }

  public function setWorkflowId($workflow_id) {
    $this->workflow_id = $workflow_id;
  }

  public function getWorkflowStateDefaultValue() {
    return $this->workflow_state_default_value;
  }

  public function setWorkflowStateDefaultValue($workflow_state_default_value) {
    $this->workflow_state_default_value = $workflow_state_default_value;
  }

  public function getAccessFieldWorkflowState() {
    return $this->access_field_workflow_state;
  }

  public function setAccessFieldWorkflowState($workflow_state_id) {
    $this->access_field_workflow_state = $workflow_state_id;
  }

  public function getAvailableWorkflows() {
    $workflow_manager = \Drupal::service('plugin.manager.workflow');
    return array_filter($workflow_manager->getDefinitions(), function($definition) {
      return isset($definition['group']) && $definition['group'] == self::WORKFLOW_GROUP_ID;
    });
  }

  public function getAvailableWorkflowOptionList() {
    return array_map(function($definition) { return $definition['label']; }, $this->getAvailableWorkflows());
  }
}
