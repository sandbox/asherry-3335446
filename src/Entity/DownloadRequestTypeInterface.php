<?php

namespace Drupal\download_request\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Download request type entities.
 */
interface DownloadRequestTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
