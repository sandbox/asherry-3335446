<?php

namespace Drupal\download_request;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\entity\EntityPermissionProvider as ContribEntityPermissionProvider;

class EntityPermissionProvider extends ContribEntityPermissionProvider {

  /**
   * {@inheritdoc}
   */
  protected function buildBundlePermissions(EntityTypeInterface $entity_type) {
    $permissions = parent::buildBundlePermissions($entity_type);
    if ($entity_type->isRevisionable()) {
      $entity_type_id = $entity_type->id();
      $bundles = $this->entityTypeBundleInfo->getBundleInfo($entity_type_id);
      $plural_label = $entity_type->getPluralLabel();

      $actions = ['revert' => 'Revert', 'delete' => 'Delete'];

      foreach ($actions as $action_name => $action_label) {
        $permissions["$action_name all {$entity_type_id} revisions"] = [
          'title' => $this->t('@action_label @type revisions', [
            '@type' => $plural_label,
            '@action_label' => $action_label,
          ]),
        ];
        foreach ($bundles as $bundle_name => $bundle_info) {
          $permissions["$action_name {$bundle_name} {$entity_type_id} revisions"] = [
            'title' => $this->t('@bundle: @action_label @type revisions', [
              '@bundle' => $bundle_info['label'],
              '@type' => $plural_label,
              '@action_label' => $action_label,
            ]),
          ];
        }
      }
    }

    return $permissions;
  }

}
