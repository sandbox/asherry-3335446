<?php

namespace Drupal\download_request\EventSubscriber;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\download_request\DownloadRequestManager;
use Drupal\download_request\Entity\DownloadRequestItemInterface;
use Drupal\state_machine\Event\WorkflowTransitionEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Response to a workflow state transition.
 */
class DownloadRequestEventSubscriber implements EventSubscriberInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The download request manager.
   *
   * @var \Drupal\download_request\DownloadRequestManager
   */
  protected $downloadRequestManager;

  /**
   * Constructs a new DownloadRequestEventSubscriber object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\download_request\DownloadRequestManager $download_request_manager
   *   The download request manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, DownloadRequestManager $download_request_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->downloadRequestManager = $download_request_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    // download_request is the group
    // transition id like "approved"
    // phase like 'pre_transition' || 'post_transition'
    $events['download_request.post_transition'] = ['downloadRequestPostTransition'];

    return $events;
  }

  /**
   * This method is called when the download_request.pre_transition is dispatched.
   *
   * @param WorkflowTransitionEvent $event
   *   The dispatched event.
   */
  public function downloadRequestPostTransition(WorkflowTransitionEvent $event) {
    $entity = $event->getEntity();
    if (!$entity instanceof DownloadRequestItemInterface) {
      return;
    }
    if (!$download_request = $entity->getParentEntity()) {
      \Drupal::logger('download_request')->error('Parent entity missing for download request item @id', [
        '@id' => $entity->id(),
      ]);
      return;
    }
    if (!$user = $download_request->getOwner()) {
      return;
    }
    $this->downloadRequestManager->refreshUserDownloads($user);
  }

}
