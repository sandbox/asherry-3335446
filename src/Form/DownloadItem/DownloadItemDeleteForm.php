<?php

namespace Drupal\download_request\Form\DownloadItem;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\ContentEntityDeleteForm;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Drupal\download_request\Entity\DownloadRequestItem;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for deleting Download item entities.
 *
 * @ingroup download_request
 */
class DownloadItemDeleteForm extends ContentEntityDeleteForm {

  /**
   * @var DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityRepositoryInterface $entity_repository, EntityTypeBundleInfoInterface $entity_type_bundle_info = NULL, TimeInterface $time = NULL, DateFormatterInterface $date_formatter) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);
    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('date.formatter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $request_items = $this->entityTypeManager
      ->getStorage('download_request_item')
      ->loadMultiple([
        'download_item.target_id' => $this->entity->id(),
      ]);

    if (!empty($request_items)) {
      $form['delete_request_items'] = [
        '#title' => $this->t('Remove items from affected download requests (@count)', [
          '@count' => count($request_items),
        ]),
        '#type' => 'checkbox',
        '#default_value' => TRUE,
      ];
      $form_state->set('request_items', $request_items);

      $form['request_items_summary'] = [
        '#type' => 'details',
        '#title' => $this->t('Affected download requests'),
        'list' => [
          '#markup' => $this->buildEntityList($request_items),
        ],
      ];
    }

    return $form;
  }

  /**
   * Build a <ul> of labels for the request item parent requests.
   *
   * @param DownloadRequestItem[] $request_items
   *
   * @return string
   */
  private function buildEntityList(array $request_items) {
    $html = '<ul>';
    foreach ($request_items as $request_item) {
      $request = $request_item->getParentEntity();
      $label = $this->t('Request by @owner on @created', [
        '@owner' => $request->getOwner() ? $request->getOwner()->getAccountName() : $request->id(),
        '@created' => \Drupal::service('date.formatter')->format($request->getCreatedTime()),
      ]);
      $html .= "<li>{$request->toLink($label)->toString()}</li>";
    }
    $html .= '</ul>';
    return $html;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->getValue('delete_request_items')) {
      $items = $form_state->get('request_items');
      foreach ($form_state->get('request_items') as $request_item) {
        if ($request = $request_item->getParentEntity()) {
          // First remove the item from the fields to avoid errors in DownloadRequest::postSave().
          $request->request_items->filter(function($item) use ($request_item) { return $item->target_id != $request_item->id(); });
          $request->save();
        }
        $request_item->delete();
      }

      // Prep the message to send after the current download item is deleted.
      $message = $this->t('@count download requests updated.', [
        '@count' => count($items),
      ]);
      $message .= $this->buildEntityList($items);
    }

    parent::submitForm($form, $form_state);

    if ($form_state->getValue('delete_request_items') && isset($message)) {
      $this->messenger()->addStatus(Markup::create($message));
    }
  }
}
