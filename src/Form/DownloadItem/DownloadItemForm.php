<?php

namespace Drupal\download_request\Form\DownloadItem;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for Download item edit forms.
 *
 * @ingroup download_request
 */
class DownloadItemForm extends ContentEntityForm {

  /**
   * The current user account.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $account;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    $instance = parent::create($container);
    $instance->account = $container->get('current_user');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var \Drupal\download_request\Entity\DownloadItem $entity */
    $form = parent::buildForm($form, $form_state);

    $form['advanced'] = [
      '#type' => 'vertical_tabs',
      '#weight' => 99,
    ];

    $form['entity_info'] = [
      '#type' => 'details',
      '#group' => 'advanced',
      '#title' => $this->t('Entity Information'),
    ];

    $form['entity_revision'] = [
      '#type' => 'details',
      '#group' => 'advanced',
      '#title' => $this->t('Entity Revision'),
    ];

    if (isset($form['path'])) {
      $form['path']['#group'] = 'entity_info';
    }

    if (isset($form['status'])) {
      $form['status']['#group'] = 'entity_info';
    }

    if (isset($form['revision_log_message'])) {
      $form['revision_log_message']['#group'] = 'entity_revision';
    }

    if (!$this->entity->isNew()) {
      $form['new_revision'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Create new revision'),
        '#default_value' => FALSE,
        '#group' => 'entity_revision',
        '#weight' => 10,
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\download_request\Entity\DownloadItem $entity */
    $entity = $this->entity;

    // Save as a new revision if requested to do so.
    if (!$form_state->isValueEmpty('new_revision') && $form_state->getValue('new_revision') != FALSE) {
      $entity->setNewRevision();

      // If a new revision is created, save the current user as revision author.
      $entity->setRevisionCreationTime($this->time->getRequestTime());
      $entity->setRevisionUserId($this->account->id());
    }
    else {
      $entity->setNewRevision(FALSE);
    }
    $status = parent::save($form, $form_state);

    $this->messenger()->addMessage($this->t(
      '@action the %label Download item.', [
        '%label' => $entity->label(),
        '@action' => $status === SAVED_NEW ? 'Created' : 'Updated',
    ]));

    $form_state->setRedirectUrl($entity->toUrl('canonical'));
  }
}
