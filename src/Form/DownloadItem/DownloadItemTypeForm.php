<?php

namespace Drupal\download_request\Form\DownloadItem;

use Drupal\Core\Entity\EntityFieldManager;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class DownloadItemTypeForm.
 */
class DownloadItemTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $download_item_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $download_item_type->label(),
      '#description' => $this->t("Label for the Download item type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $download_item_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\download_request\Entity\DownloadItemType::load',
      ],
      '#disabled' => !$download_item_type->isNew(),
    ];

    /** @var EntityFieldManager $entity_field_manager */
    $entity_field_manager = \Drupal::service('entity_field.manager');
    $content_entity_type_id = $this->entity->getEntityType()->getBundleOf();
    $fields = $entity_field_manager->getFieldDefinitions($content_entity_type_id, $this->entity->id());

    $form['access_field'] = [
      '#title' => t('Access field'),
      '#type' => 'select',
      '#description' => $this->t('Set which field will require approval for users to view.'),
      '#options' => array_map(function($field) {
        return $field->getLabel();
      }, $fields),
      '#default_value' => $this->entity->getAccessField(),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $download_item_type = $this->entity;
    $status = $download_item_type->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Download item type.', [
          '%label' => $download_item_type->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Download item type.', [
          '%label' => $download_item_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($download_item_type->toUrl('collection'));
  }

}
