<?php

namespace Drupal\download_request\Form\DownloadRequest;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Download request entities.
 *
 * @ingroup download_request
 */
class DownloadRequestDeleteForm extends ContentEntityDeleteForm {


}
