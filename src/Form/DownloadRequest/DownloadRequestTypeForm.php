<?php

namespace Drupal\download_request\Form\DownloadRequest;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\state_machine\WorkflowManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class DownloadRequestTypeForm.
 *
 * TODO: We should add another setting if we need to specify the DownloadItem bundles to limit to.
 *       DownloadRequest bundles might want to only reference specific DownloadItem bundles.
 */
class DownloadRequestTypeForm extends EntityForm {

  protected $workflowManager;

  /**
   * DownloadRequestTypeForm constructor.
   */
  public function __construct(WorkflowManagerInterface $workflow_manager) {
    $this->workflowManager = $workflow_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('plugin.manager.workflow'));
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $download_request_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $download_request_type->label(),
      '#description' => $this->t("Label for the Download request type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $download_request_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\download_request\Entity\DownloadRequestType::load',
      ],
      '#disabled' => !$download_request_type->isNew(),
    ];

    $workflow_id = $download_request_type->getWorkflowId();

    $form['workflow_id'] = [
      '#type' => 'select',
      '#title' => $this->t('Workflow'),
      '#description' => $this->t('The workflow plugin to use for the DownloadRequestItem state field.'),
      '#options' => $download_request_type->getAvailableWorkflowOptionList(),
      '#default_value' => $download_request_type->getWorkflowId(),
    ];

    $form['access_field_workflow_state'] = [
      '#title' => $this->t('Dowload item asset state'),
      '#type' => 'select',
      '#options' => $workflow_id ? $this->workflowManager->createInstance($workflow_id)->getStates() : [],
      '#default_value' => $download_request_type->getAccessFieldWorkflowState(),
    ];

    $form['workflow_state_default_value'] = [
      '#title' => $this->t('Workflow state default value'),
      '#type' => 'select',
      '#options' => $workflow_id ? $this->workflowManager->createInstance($workflow_id)->getStates() : [],
      '#default_value' => $download_request_type->getWorkflowStateDefaultValue(),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $download_request_type = $this->entity;
    $status = $download_request_type->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Download request type.', [
          '%label' => $download_request_type->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Download request type.', [
          '%label' => $download_request_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($download_request_type->toUrl('collection'));
  }

}
