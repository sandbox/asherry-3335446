<?php

namespace Drupal\download_request\Plugin\Condition;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Annotation\ContextDefinition;
use Drupal\Core\Condition\ConditionPluginBase;
use Drupal\download_request\DownloadRequestManager;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Define the download item user access condition.
 *
 * @Condition(
 *   id = "download_item_user_access",
 *   label = @Translation("Download Item User Access"),
 *   context_definitions = {
 *     "download_item" = @ContextDefinition("entity:download_item", label = @Translation("Download Item"))
 *   }
 * )
 */
class DownloadItemUserAccess extends ConditionPluginBase implements ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\download_request\DownloadRequestManager
   */
  protected $downloadRequestManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    DownloadRequestManager $download_request_manager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->downloadRequestManager = $download_request_manager;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('download_request.manager')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $configuration = $this->getConfiguration();

    $form['check_access'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Check User Access'),
      '#description' => $this->t('If checked, the current user will be checked
        to ensure they have access to the download item.'),
      '#default_value' => $configuration['check_access'] ?? FALSE,
    ];

    return parent::buildConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritDoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $this->configuration['check_access'] = $form_state->getValue('check_access');
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * Check for negation in configuration and add to rseltu.
   *
   * @param $result
   *   Boolean returned from ::evaluate().
   *
   * @return bool
   */
  private function doEval($result): bool {
    $negate = $this->configuration['negate'] ?? 0;
    return $negate ? !$result : $result;
  }

  public function getCacheContexts() {
    $contexts = parent::getCacheContexts();
    $contexts[] = 'user';
    return $contexts;
  }

  /**
   * {@inheritDoc}
   */
  public function evaluate(): bool {
    try {
      $account = \Drupal::currentUser();

      if ($account->isAnonymous()) {
        return $this->doEval(FALSE);
      }

      if (
        ($context = $this->getContext('download_item'))
        && $context->hasContextValue()
      ) {
        $result = $this->downloadRequestManager->userHasDownload(
          $context->getContextValue(),
          $account
        );
        return $this->doEval($result);
      }
    } catch (\Exception $exception) {
      watchdog_exception('download_request', $exception);
    }

    // Leave out negate for now because this will have been an error.
    return FALSE;
  }

  /**
   * {@inheritDoc}
   */
  public function summary() {
    return $this->t(
      'Check access for the download request against the current user.'
    );
  }
}
