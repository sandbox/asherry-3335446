<?php

namespace Drupal\download_request\Plugin\views\field;

use Drupal\views\Plugin\views\field\BulkForm;

/**
 * @ViewsField("download_request_bulk_form")
 */
class DownloadRequestBulkForm extends BulkForm {

}
