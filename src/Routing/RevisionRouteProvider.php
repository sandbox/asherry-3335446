<?php

namespace Drupal\download_request\Routing;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Routing\EntityRouteProviderInterface;
use Drupal\entity\Routing\RevisionRouteProvider as ContribRevisionRouteProvider;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Provides revision routes.
 */
class RevisionRouteProvider extends ContribRevisionRouteProvider {

  /**
   * {@inheritdoc}
   */
  public function getRoutes(EntityTypeInterface $entity_type) {
    $collection = parent::getRoutes($entity_type);

    $entity_type_id = $entity_type->id();

    if ($view_route = $this->getRevisionDeleteRoute($entity_type)) {
      $collection->add("entity.$entity_type_id.revision_delete_form", $view_route);
    }

    return $collection;
  }

  /**
   * Gets the entity revision delete route.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getRevisionDeleteRoute(EntityTypeInterface $entity_type) {
    if ($entity_type->hasLinkTemplate('revision-delete-form')) {
      $entity_type_id = $entity_type->id();
      if ($entity_type_id === 'download_request') {
        $form = '\Drupal\download_request\Form\DownloadRequest\DownloadRequestRevisionDeleteForm';
      }
      elseif ($entity_type_id === 'download_item') {
        $form = '\Drupal\download_request\Form\DownloadItem\DownloadItemRevisionDeleteForm';
      }
      else {
        return NULL;
      }
      $route = new Route($entity_type->getLinkTemplate('revision-delete-form'));

      $route->addDefaults([
        '_form' => $form,
        'title' => 'Delete current revision',
      ]);
      $route->addRequirements([
        '_entity_access_revision' => "$entity_type_id.update",
      ]);
      $route->setOption('parameters', [
        $entity_type->id() => [
          'type' => 'entity:' . $entity_type->id(),
        ],
        $entity_type->id() . '_revision' => [
          'type' => 'entity_revision:' . $entity_type->id(),
        ],
      ]);
      return $route;
    }
  }
}
